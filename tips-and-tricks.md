## The Vim goto line number command(https://alvinalexander.com/linux-unix/vim-goto-line-number-go-to-vi)
```
G   - go to the last line in the vim editor (last line in the file)
1G  - goto line number 1
20G - goto line number 20
```

## Duplicate a whole line in Vim
```
yy or Y to copy the line 
or 
dd to delete (cutting) the line

then

p to paste the copied or deleted text after the current line 
or 
P to paste the copied or deleted text before the current line
```


## Docker
###Finding a string in docker logs of container
```
docker logs nginx 2>&1 | grep "127."
```
### Inspect docker network contain
```
docker network inspect <network name>
```

### Docker ps: uniquement les noms des conteneurs
```
docker ps --format "{{.Names}}"

docker ps --format 'table {{.Ports}}\t{{.Names}}'
```

### Docker rm: all running containers
```
docker rm -f $(docker ps -aq)
```

### [logspout](https://github.com/gliderlabs/logspout#logspout)
logspout est un routeur de log pour les conteurs dockers qui tourne dans docker. Il s'attache � tous les conteneurs d'une machine et route leurs logs
o� on le d�sire. Il a aussi un module d'extension. Pour l'instant il ne capture  que les soties stdout et stderr, mais un module pour la collection syslog est plannifi�.
#### Installation
```
docker pull gliderlabs/logspout:latest
```
#### Utilisation
More information at https://github.com/gliderlabs/logspout/tree/master/httpstream

Define PORT and DOCKER_NETWORK environment variables.


```
PORT="8000"
DOCKER_NETWORK="net_byfn"

docker kill logspout 2> /dev/null 1>&2 || true
docker rm logspout 2> /dev/null 1>&2 || true

docker run -d --name="logspout" \
	--volume=/var/run/docker.sock:/var/run/docker.sock \
	--publish=127.0.0.1:${PORT}:80 \
	--network  ${DOCKER_NETWORK} \
	gliderlabs/logspout
sleep 3
curl http://127.0.0.1:${PORT}/logs
```


## Hyperledger Composer
composer-rest-server -c admin@tutorial-network -n never -u true -w true

## [GNU Screen](https://doc.ubuntu-fr.org/screen)
> GNU Screen est un multiplexeur de terminaux permettant d'ouvrir plusieurs terminaux dans une m�me console, de passer de l'un � l'autre et de les r�cup�rer plus tard.

### Cr�er une session Screen
```
screen -S nom_de_la_session
```

### Lister les sessions Screen
```
screen -ls
```

### Supprimer une session Screen morte
```
screen -wipe
```

### Se d�tacher de la session du screen
```
[CTRL]+[a] suivi de [d]
```
### Se rattacher � la session
```
screen -r nom_de_la_session
```

### Gestion des terminaux
#### Cr�er un terminal
```
[CTRL]+[a] suivi de [c]
```

#### Lister des diff�rents terminaux, avec la possibilit� d'en choisir un
```
[CTRL]+[a] suivi de ["]
```

#### Nommer les terminaux et s'y rendre par la suite plus ais�ment
```
[CTRL]+[a] suivi de [A]
```

#### S�pare la console courante en deux consoles verticalement (la console courante devient celle de gauche)
```
CTRL]+[a] suivi de [AltGr]+[6] (|):
```

#### S�pare la console courante en deux consoles horizontalement (la console courante devient celle du dessus)
```
[CTRL]+[a] suivi de [S]
```

#### Ferme la r�gion courante(ne supprime pas la console qui s'y trouvait)
```
[CTRL]+[a] suivi de [X]
```

### Tuer une session 
```
screen -X -S [session # you want to kill] quit
```










