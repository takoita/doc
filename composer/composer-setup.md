
# Installing the development environment
https://hyperledger.github.io/composer/latest/installing/development-tools.html

## Installing pre-requisites
https://hyperledger.github.io/composer/latest/installing/installing-prereqs.html

```
curl -O https://hyperledger.github.io/composer/latest/prereqs-ubuntu.sh

chmod u+x prereqs-ubuntu.sh

./prereqs-ubuntu.sh
```
## Installing components
### Step 1: Install the CLI tools
```
npm install -g composer-cli@0.20

npm install -g composer-rest-server@0.20

npm install -g generator-hyperledger-composer@0.20

npm install -g yo
```
### Step 2: Install Playground
```
npm install -g composer-playground@0.20
```
### Step 3: Set up your IDE
1. Install VSCode from this URL: https://code.visualstudio.com/download
2. Open VSCode, go to Extensions, then search for and install the Hyperledger Composer extension from the Marketplace.

### Step 4: Install Hyperledger Fabric
1. In a directory of your choice, get the .tar.gz file that contains the tools to install Hyperledger Fabric:
```
curl -O https://raw.githubusercontent.com/hyperledger/composer-tools/master/packages/fabric-dev-servers/fabric-dev-servers.tar.gz
tar -xvf fabric-dev-servers.tar.gz
```
2. Use the scripts you just downloaded and extracted to download a local Hyperledger Fabric v1.2 runtime:
```
export FABRIC_VERSION=hlfv12
./downloadFabric.sh
```

## Controlling your dev environment
### Starting and stopping Hyperledger Fabric
The first time you start up a new runtime, you'll need to run the start script, then generate a PeerAdmin card:
```
export FABRIC_VERSION=hlfv12
./startFabric.sh
./createPeerAdminCard.sh
```

You can start and stop your runtime
```
~/fabric-dev-servers/stopFabric.sh

~/fabric-dev-servers/startFabric.sh
```
At the end of your development session, you run:
```
~/fabric-dev-servers/stopFabric.sh

~/fabric-dev-servers/teardownFabric.sh
```
Note that if you've run the teardown script, the next time you start the runtime, you'll need to create a new PeerAdmin card just like you did on first time startup.

### Start the web app ("Playground")
```
composer-playground
```
It will typically open your browser automatically, at the following address: http://localhost:8080/login.

You should see the PeerAdmin@hlfv1 Card you created with the createPeerAdminCard script on your "My Business Networks" screen in the web app: if you don't see this, you may not have correctly started up your runtime!
