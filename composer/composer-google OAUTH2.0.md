# Using Google OAUTH2.0 with a REST server

## Step 1: Set up the Persistent DB Credentials Data Store using MongoDB

### Start the MongoDB Instance
```docker run -d --name mongo --network composer_default -p 27017:27017 mongo```

## Step 2: Build the REST Server Docker Image with OAUTH2.0 module

``` 
mkdir dockertmp
cd dockertmp
```
Create Dockerfile with content
```
FROM hyperledger/composer-rest-server
RUN npm install --production loopback-connector-mongodb passport-google-oauth2 && \
npm cache clean --force && \
ln -s node_modules .node_modules
```
## Step 3: Define Environment variables for REST Server instance configuration
Put in the file envvars.txt:
```
COMPOSER_CARD=restadmin@trade-network
COMPOSER_NAMESPACES=never
COMPOSER_AUTHENTICATION=true
COMPOSER_MULTIUSER=true
COMPOSER_PROVIDERS='{
    "google": {
        "provider": "google",
        "module": "passport-google-oauth2",
        "clientID": "312039026929-t6i81ijh35ti35jdinhcodl80e87htni.apps.googleusercontent.com",
        "clientSecret": "Q4i_CqpqChCzbE-u3Wsd_tF0",
        "authPath": "/auth/google",
        "callbackURL": "/auth/google/callback",
        "scope": "https://www.googleapis.com/auth/plus.login",
        "successRedirect": "/",
        "failureRedirect": "/"
    }
}'
COMPOSER_DATASOURCES='{
    "db": {
        "name": "db",
        "connector": "mongodb",
        "host": "mongo"
    }
}'
```
## Step 4: Load environment variables in current terminal and launch the persistent REST Server instance
```
source envvars.txt
```

## Step 5: Deploy the sample Commodities Trading Business network to query from REST client
1. If you've not already done so - download the trade-network.bna for the Trade-network from https://composer-playground.mybluemix.net/.
2. In Playground, connect to the network as admin and export the trade-network.bna and copy it to your home directory.
3. To deploy the business network, first you need to install the business network onto the peers:
```
composer network install --card PeerAdmin@hlfv1 --archiveFile trade-network.bna
```

Make a note of the version that is output after running the above command. This is the version of the business network which you will provide to the next command that you use to start the business network.

4. run the following commands, replacing <business_network_version> with the version number output from the previous install command:
```
composer network start --card PeerAdmin@hlfv1 --networkName trade-network --networkVersion <business_network_version> --networkAdmin admin --networkAdminEnrollSecret adminpw --file networkadmin.card
```

5. Next, import the business network card, and connect with the card to download the certs to the wallet:
```
composer card import -f networkadmin.card
composer network ping -c admin@trade-network
```

## Step 6: Create the REST server Administrator for the Composer REST server instance
1. Create a REST Administrator identity called restadmin and an associated business network card (used to launch the REST server later).
```
composer participant add -c admin@trade-network -d '{"$class":"org.hyperledger.composer.system.NetworkAdmin", "participantId":"restadmin"}'
composer identity issue -c admin@trade-network -f restadmin.card -u restadmin -a "resource:org.hyperledger.composer.system.NetworkAdmin#restadmin"
```

2. Import and test the card:
```
composer card import -f  restadmin.card
composer network ping -c restadmin@trade-network
```

3. Because we are hosting our REST server in another location with its own specific network IP information, we need to update the connection.json - so that the docker hostnames (from within the persistent REST server instance) can resolve each other's IP addresses.
```
sed -e 's/localhost:7051/peer0.org1.example.com:7051/' -e 's/localhost:7053/peer0.org1.example.com:7053/' -e 's/localhost:7054/ca.org1.example.com:7054/'  -e 's/localhost:7050/orderer.example.com:7050/'  < $HOME/.composer/cards/restadmin@trade-network/connection.json  > /tmp/connection.json && cp -p /tmp/connection.json $HOME/.composer/cards/restadmin@trade-network/ 
```

## Step 7: Launch the persistent REST server instance
1. Run the following docker command to launch a REST server instance (with the restadmin business network card)
```
docker run \
-d \
-e COMPOSER_CARD=${COMPOSER_CARD} \
-e COMPOSER_NAMESPACES=${COMPOSER_NAMESPACES} \
-e COMPOSER_AUTHENTICATION=${COMPOSER_AUTHENTICATION} \
-e COMPOSER_MULTIUSER=${COMPOSER_MULTIUSER} \
-e COMPOSER_PROVIDERS="${COMPOSER_PROVIDERS}" \
-e COMPOSER_DATASOURCES="${COMPOSER_DATASOURCES}" \
-v ~/.composer:/home/composer/.composer \
--name rest \
--network composer_default \
-p 3000:3000 \
myorg/composer-rest-server
```

2. Check that all is ok with our container - you can see that it is running using the following commands:
```
docker ps |grep rest
docker logs rest
```

Look for " Browse your REST API at http://localhost:3000/explorer " at the end of the log - and retrace steps (above) if not there.

## Step 8: Test the REST APIs are protected and require authorization
Open a browser window and launch the REST API explorer by going to http://localhost:3000/explorer to view and use the available APIs.

INFO Admin identity restadmin is used as an initial default - The REST server uses restadmin identity until a specific identity e.g. jdoe is set as a default identity in the REST client wallet.

1. Go to the “System: general business network methods” section
2. Go to the “/system/historian” API and click on “Try it out!” button as shown below:
3. You should get an Authorized error and that is because we have configured a Google+ passport OAUTH2.0 authentication strategy to protect access to the REST server. Once authentication via the OAUTH2.0 authentication pa has been achieved, the REST APIs in the browser can interact with the Trade Commodity business network (ie. once a business card has been imported).

## Step 9: Create some Participants and Identities for testing OAUTH2.0 authentication
1. You need to create a set participant and identities for testing you can interact with the business network. This is because the REST server can handle multiple REST clients in multi-user mode. We will be using the composer CLI commands to add participants and identities as follows - first name is Jo Doe:
```
composer participant add -c admin@trade-network -d '{"$class":"org.example.trading.Trader","tradeId":"trader1", "firstName":"Jo","lastName":"Doe"}'
composer identity issue -c admin@trade-network -f jdoe.card -u jdoe -a "resource:org.example.trading.Trader#trader1"
composer card import -f jdoe.card
```

2. Once again, because we will use this identity to test inside the persistent REST docker container - we will need to change the hostnames to represent the docker resolvable hostnames - once again run this one-liner to carry out those changes quickly:
```
sed -e 's/localhost:7051/peer0.org1.example.com:7051/' -e 's/localhost:7053/peer0.org1.example.com:7053/' -e 's/localhost:7054/ca.org1.example.com:7054/'  -e 's/localhost:7050/orderer.example.com:7050/'  < $HOME/.composer/cards/jdoe@trade-network/connection.json  > /tmp/connection.json && cp -p /tmp/connection.json $HOME/.composer/cards/jdoe@trade-network/ 
```

3. We need to export the card to a file - to use for importing elsewhere - ie the card that we will use to import to the wallet in our browser client - and therefore at this point, we can discard the initial business network card file for jdoe.
```
composer card export -f jdoe_exp.card -c jdoe@trade-network ; rm jdoe.card
```

3. Repeat the above steps for participant Ken Coe (kcoe) - creating a trader2 participant and issuing the identity kcoe - the sequence of commands are:
```
composer participant add -c admin@trade-network -d '{"$class":"org.example.trading.Trader","tradeId":"trader2", "firstName":"Ken","lastName":"Coe"}'
composer identity issue -c admin@trade-network -f kcoe.card -u kcoe -a "resource:org.example.trading.Trader#trader2"
composer card import -f kcoe.card

sed -e 's/localhost:7051/peer0.org1.example.com:7051/' -e 's/localhost:7053/peer0.org1.example.com:7053/' -e 's/localhost:7054/ca.org1.example.com:7054/'  -e 's/localhost:7050/orderer.example.com:7050/'  < $HOME/.composer/cards/kcoe@trade-network/connection.json  > /tmp/connection.json && cp -p /tmp/connection.json $HOME/.composer/cards/kcoe@trade-network/ 

composer card export -f kcoe_exp.card -c kcoe@trade-network ; rm kcoe.card
```

These cards can now be imported, then used into the REST client (ie the browser) in the next section.

## Step 10: Authenticating from the REST API Explorer and testing using specific identities

1. Go to http://localhost:3000/auth/google - this will direct you to the Google Authentication consent screen.

2. You will be authenticated by Google and be redirected back to the REST server (http://localhost:3000/explorer) which shows the access token message in the top left-hand corner - click on 'Show' to view the token.

While our REST server has authenticated to Google+ OAUTH2.0 service (defined by its project/client scope, and using the client credentials set up in the Appendix for our OAUTH2.0 service), we have not actually done anything yet, in terms of blockchain identity or using business network cards to interact with our Trade Commodity business network - we will do that next, using the jdoe identity we created earlier.

## Step 11: Check the Default Wallet and Import the card and set a default Identity
1. Firstly, go to the REST endpoint under Wallets and do a GET operation (and 'Try it Out') to get the contents of the Wallet - check that the wallet does not contain any business network cards - it should show as empty [ ]:
2. You need to add an identity to the REST client wallet and then set this identity as the default one to use for making API calls. Go to the POST system operation under /Wallets - its called the /Wallets/Import endpoint
3. Choose to import the file jdoe_exp.card - and provide the name of the card as jdoe@trade-network and click 'Try it Out'
4. Scroll down - you should you should get an HTTP Status code 204 (request was successful).
5. Next, go back to GET /wallets.
You should see that jdoe@trade-network is imported into the wallet. Note also that the value of the default property is true, which means that this business network card will be used by default when interacting with the Commodity Trading business network (until such time as you change it to use another).

## Test interaction with the Business Network as the default ID jdoe
1. Go to System REST API Methods section and expand the /GET System/Historian section. Click on 'Try It Out' - you should now see results from the Historian Registry, as the blockchain identity 'jdoe' and a set of transactions
2. Go to the Trader methods and expand the /GET Trader endpoint then click 'Try it Out'. It should confirm that we are able to interact with the REST Server as jdoe in our authenticated session.
3. Next, return to the POST /wallet/import operation and import the card file kcoe_exp.card with the card name set to kcoe@trade-network and click on 'Try it Outto import it - it should return a successful (204) response.
4. However, to use this card, we need to set it as the default card name in our Wallet - go to the POST /wallet/name/setDefault/ method and choose kcoe@trade-network as the default card name and click on Try it Out. This is now the default card.
5. Return to the Trader methods and expand the /GET Trader endpoint then click 'Try it Out' . It should confirm that we are now using a different card name and still be able to interact with the REST Server as we are still authenticated.

## Conclusion
This concludes the tutorial section - you've seen how to configure a client-based Google OAUTH2.0 authentication service that can be used to authorize client applications and provide consent to interact with a protected resource server (namely an REST Server instance) without the need to authenticate on every request. Furthermore, the REST Server is running in multi-user mode and so, allows multiple blockchain identities to interact with the deployed Commodity Trading business network from the same REST client session, subject to token expiration times etc etc.

