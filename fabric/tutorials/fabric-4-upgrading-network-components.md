# Upgrading Your Network Components
Because the Building Your First Network (BYFN) tutorial defaults to the “latest” binaries, 
if you have run it since the release of v1.4, your machine will have v1.4 binaries and tools 
installed on it and you will not be able to upgrade them.

As a result, this tutorial will provide a network based on Hyperledger Fabric v1.3 binaries 
as well as the v1.4 binaries you will be upgrading to.

At a high level, our upgrade tutorial will perform the following steps:
1. Backup the ledger and MSPs.
2. Upgrade the orderer binaries to Fabric v1.4.
3. Upgrade the peer binaries to Fabric v1.4.

## Launch a v1.3 network
Before we can upgrade to v1.4, we must first provision a network running Fabric v1.3 images.

Just as in the BYFN tutorial, we will be operating from the first-network subdirectory within your local clone of fabric-samples.

### Clean up
```
./byfn.sh down
```

Switch to branch v1.3.0, generate the crypto and bring up the network
```
git fetch origin

git reset --hard 

git checkout v1.3.0

./byfn.sh generate

./byfn.sh up -t 3000 -i 1.3.0

```

### Get the newest samples
```
git fetch origin

git checkout v1.4.0-rc2
```

### Upgrade the orderer containers
Orderer containers should be upgraded in a rolling fashion (one at a time). At a high level, the orderer upgrade process goes as follows:

1. Stop the orderer.
2. Back up the orderer’s ledger and MSP.
3. R2estart the orderer with the latest images.
4. Verify upgrade completion.

As a consequence of leveraging BYFN, we have a solo orderer setup, therefore, we will only perform this process once. In a Kafka setup, however,
this process will have to be repeated on each orderer.

Let’s begin the upgrade process by bringing down the orderer:
```
docker stop orderer.example.com

export LEDGERS_BACKUP=./ledgers-backup

# Note, replace '1.4.x' with a specific version, for example '1.4.0'.
# Set IMAGE_TAG to 'latest' if you prefer to default to the images tagged 'latest' on your system.

export IMAGE_TAG=$(go env GOARCH)-1.4.0-rc2
```

Backup the orderer's ledger and MSP:
```
mkdir -p $LEDGERS_BACKUP

docker cp orderer.example.com:/var/hyperledger/production/orderer/ ./$LEDGERS_BACKUP/orderer.example.com
```
In a production network this process would be repeated for each of the Kafka-based orderers in a rolling fashion.

Now download and restart the orderer with our new fabric image:
```
docker-compose -f docker-compose-cli.yaml up -d --no-deps orderer.example.com
```

Because our sample uses a “solo” ordering service, there are no other orderers in the network that the restarted orderer must sync up to.
However, in a production network leveraging Kafka, it will be a best practice to issue peer channel fetch <blocknumber> after restarting
the orderer to verify that it has caught up to the other orderers.

### Upgrade the peer containers
Peer containers should, like the orderers, be upgraded in a rolling fashion (one at a time).
. At a high level, we will perform the following steps:
1. Stop the peer.
2. Back up the peer’s ledger and MSP.
3. Remove chaincode containers and images.
4. Restart the peer with latest image.
5. Verify upgrade completion.

We have four peers running in our network. We will perform this process once for each peer, totaling four upgrades.
Let’s bring down the first peer with the following command:
```
export PEER=peer0.org1.example.com

docker stop $PEER
```

We can then backup the peer’s ledger and MSP:
```
mkdir -p $LEDGERS_BACKUP

docker cp $PEER:/var/hyperledger/production ./$LEDGERS_BACKUP/$PEER
```
With the peer stopped and the ledger backed up, remove the peer chaincode containers:
```
CC_CONTAINERS=$(docker ps | grep dev-$PEER | awk '{print $1}')
if [ -n "$CC_CONTAINERS" ] ; then docker rm -f $CC_CONTAINERS ; fi
```

And the peer chaincode images:
```
CC_IMAGES=$(docker images | grep dev-$PEER | awk '{print $1}')
if [ -n "$CC_IMAGES" ] ; then docker rmi -f $CC_IMAGES ; fi
```

Now we’ll re-launch the peer using the v1.4 image tag:
```
docker-compose -f docker-compose-cli.yaml up -d --no-deps $PEER
```

### Verify peer upgrade completion
We’ve completed the upgrade for our first peer, but before we move on let’s check to ensure the 
upgrade has been completed properly with a chaincode invoke.

> Before you attempt this, you may want to upgrade peers from enough organizations to satisfy your endorsement policy.
> Although, this is only mandatory if you are updating your chaincode as part of the upgrade process. If you are not updating your chaincode 
> as part of the upgrade process, it is possible to get endorsements from peers running at different Fabric versions.

Before we get into the CLI container and issue the invoke, make sure the CLI is updated to the most current version by issuing:
```
docker-compose -f docker-compose-cli.yaml stop cli

docker-compose -f docker-compose-cli.yaml up -d --no-deps cli
```
Once you have the version of the CLI you want, get into the CLI container:
```
docker exec -it cli bash
```

Now you’ll need to set two environment variables — the name of the channel and the name of the ORDERER_CA:
```
CH_NAME=mychannel

ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
```

Now you can issue the invoke:
```
peer chaincode invoke -o orderer.example.com:7050 --peerAddresses peer0.org1.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses peer0.org2.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt --tls --cafile $ORDERER_CA  -C $CH_NAME -n mycc -c '{"Args":["invoke","a","b","10"]}'
```

Our query earlier revealed a to have a value of 90 and we have just removed 10 with our invoke. Therefore, a query against a should reveal 80. Let’s see:
```
peer chaincode query -C mychannel -n mycc -c '{"Args":["query","a"]}'
```

After verifying the peer was upgraded correctly, make sure to issue an exit to leave the container before continuing to upgrade your peers. You can do this by repeating the process above with a different peer name exported.

## Upgrading components BYFN does not support
Although this is the end of our update tutorial, there are other components that exist in production networks that are not compatible with the BYFN sample. In this section, we’ll talk through the process of updating them.

### Fabric CA container
To learn how to upgrade your Fabric CA server, click over to the [CA documentation](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#upgrading-the-server).

### Upgrade Node SDK clients

> Upgrade Fabric and Fabric CA before upgrading Node SDK clients. Fabric and Fabric CA are tested for backwards compatibility with older SDK clients. While newer SDK clients often work with older Fabric and Fabric CA releases, they may expose features that are not yet available in the older Fabric and Fabric CA releases, 
> and are not tested for full compatibility.

Use NPM to upgrade any Node.js client by executing these commands in the root directory of your application:
```
npm install fabric-client@latest

npm install fabric-ca-client@latest
```
These commands install the new version of both the Fabric client and Fabric-CA client and write the new versions package.json.

### Upgrading the Kafka cluster
It is not required, but it is recommended that the Kafka cluster be upgraded and kept up to date along with the rest of Fabric. Newer versions of Kafka support older protocol versions, so you may upgrade Kafka before or after the rest of Fabric.

Refer to the official Apache Kafka documentation on [upgrading Kafka from previous versions](https://kafka.apache.org/documentation/#upgrade) to upgrade the Kafka cluster brokers.

### Upgrading Zookeeper
An Apache Kafka cluster requires an Apache Zookeeper cluster. The Zookeeper API has been stable for a long time and, as such, almost any version of Zookeeper is tolerated by Kafka. Refer to the Apache Kafka upgrade documentation in case there is a specific requirement to upgrade to a specific version of Zookeeper.

### Upgrading CouchDB
If you are using CouchDB as state database, you should upgrade the peer’s CouchDB at the same time the peer is being upgraded. CouchDB v2.2.0 has been tested with Fabric v1.4.

To upgrade CouchDB:
1. Stop CouchDB.
2. Backup CouchDB data directory.
3. Install CouchDB v2.2.0 binaries or update deployment scripts to use a new Docker image (CouchDB v2.2.0 pre-configured Docker image is provided alongside Fabric v1.4).
4. Restart CouchDB.

### Upgrade Node chaincode shim
To move to the new version of the Node chaincode shim a developer would need to:

1. Change the level of fabric-shim in their chaincode package.json from 1.3 to 1.4.
2. Repackage this new chaincode package and install it on all the endorsing peers in the channel.
2. Perform an upgrade to this new chaincode. To see how to do this, check out peer chaincode.

> This flow isn’t specific to moving from 1.3 to 1.4. It is also how one would upgrade from any incremental version of the node fabric shim.

### Upgrade Chaincodes with vendored shim
> The v1.3.0 shim is compatible with the v1.4 peer, but, it is still best practice to upgrade the chaincode shim to match the current level of the peer.

A number of third party tools exist that will allow you to vendor a chaincode shim. If you used one of these tools, use the same one to update your vendoring and re-package your chaincode.

If you did not vendor your chaincode, you can skip this step entirely.











































