
# Using Private Data in Fabric

This tutorial will demonstrate the use of collections to provide storage and retrieval of private data on the blockchain network for authorized peers of organizations.

The tutorial will take you through the following steps to practice defining, configuring and using private data with Fabric:

1. Build a collection definition JSON file
2. Read and Write private data using chaincode APIs
3. Install and instantiate chaincode with a collection
3. Store private data
4. Query the private data as an authorized peer
5. Query the private data as an unauthorized peer
6. Purge Private Data
7. Using indexes with private data
8. Additional resources

## Build a collection definition JSON file
The first step in privatizing data on a channel is to build a collection definition which defines access to the private data. The collection definition describes:
 * who can persist data
 * how many peers the data is distributed to
 * how many peers are required to disseminate the private data
 * and how long the private data is persisted in the private database.
 
A collection definition is composed of the following properties:
 * **name**: Name of the collection.
 * **policy**: Defines the organization peers allowed to persist the collection data.
 * **requiredPeerCount**: Number of peers required to disseminate the private data as a condition of the endorsement of the chaincode
 * **maxPeerCount**: For data redundancy purposes, the number of other peers that the current endorsing peer will attempt to distribute the data to. If an endorsing peer goes down, these other peers are available at commit time if there are requests to pull the private data.
 * **blockToLive**: For very sensitive information such as pricing or personal information, this value represents how long the data should live on the private database in terms of blocks. The data will live for this specified number of blocks on the private database and after that it will get purged, making this data obsolete from the network. To keep private data indefinitely, that is, to never purge private data, set the blockToLive property to 0.
 * **memberOnlyRead**: a value of true indicates that peers automatically enforce that only clients belonging to one of the collection member organizations are allowed read access to private data.

To illustrate usage of private data, the marbles private data example contains two private data collection definitions: `collectionMarbles` and `collectionMarblePrivateDetails`.

```
// collections_config.json
[
  {
       "name": "collectionMarbles",
       "policy": "OR('Org1MSP.member', 'Org2MSP.member')",
       "requiredPeerCount": 0,
       "maxPeerCount": 3,
       "blockToLive":1000000,
       "memberOnlyRead": true
  },

  {
       "name": "collectionMarblePrivateDetails",
       "policy": "OR('Org1MSP.member')",
       "requiredPeerCount": 0,
       "maxPeerCount": 3,
       "blockToLive":3,
       "memberOnlyRead": true
  }
]
```
The data to be secured by these policies is mapped in chaincode and will be shown later in the tutorial.

## Read and Write private data using chaincode APIs
The next step in understanding how to privatize data on a channel is to build the data definition in the chaincode. The marbles private data sample divides the private data into two separate data definitions according to how the data will be accessed.
```
// Peers in Org1 and Org2 will have this private data in a side database
type marble struct {
  ObjectType string `json:"docType"`
  Name       string `json:"name"`
  Color      string `json:"color"`
  Size       int    `json:"size"`
  Owner      string `json:"owner"`
}

// Only peers in Org1 will have this private data in a side database
type marblePrivateDetails struct {
  ObjectType string `json:"docType"`
  Name       string `json:"name"`
  Price      int    `json:"price"`
}
```
 
### Reading collection data
Use the chaincode API `GetPrivateData()` to query private data in the database. `GetPrivateData()`takes two arguments, the **collection name** and the data key.

### Writing private data
Use the chaincode API `PutPrivateData()` to store the private data into the private database. Since the marbles private data sample includes two different collections, it is called twice in the chaincode:
1.  Write the private data  `name,  color,  size  and  owner`  using the collection named  `collectionMarbles`.
2.  Write the private data  `price`  using the collection named  `collectionMarblePrivateDetails`.

For example, in the following snippet of the `initMarble` function, `PutPrivateData()` is called twice, once for each set of private data:

```
// ==== Create marble object, marshal to JSON, and save to state ====
      marble := &marble{
              ObjectType: "marble",
              Name:       marbleInput.Name,
              Color:      marbleInput.Color,
              Size:       marbleInput.Size,
              Owner:      marbleInput.Owner,
      }
      marbleJSONasBytes, err := json.Marshal(marble)
      if err != nil {
              return shim.Error(err.Error())
      }

      // === Save marble to state ===
      err = stub.PutPrivateData("collectionMarbles", marbleInput.Name, marbleJSONasBytes)
      if err != nil {
              return shim.Error(err.Error())
      }

      // ==== Create marble private details object with price, marshal to JSON, and save to state ====
      marblePrivateDetails := &marblePrivateDetails{
              ObjectType: "marblePrivateDetails",
              Name:       marbleInput.Name,
              Price:      marbleInput.Price,
      }
      marblePrivateDetailsBytes, err := json.Marshal(marblePrivateDetails)
      if err != nil {
              return shim.Error(err.Error())
      }
      err = stub.PutPrivateData("collectionMarblePrivateDetails", marbleInput.Name, marblePrivateDetailsBytes)
      if err != nil {
              return shim.Error(err.Error())
      }
```

## Start the network
Now we are ready to step through some commands which demonstrate using private data.

The following command will kill any active or stale docker containers and remove previously generated artifacts. Therefore let’s run the following command to clean up any previous environments:
```
cd fabric-samples/first-network
./byfn.sh down
```
If you’ve already run through this tutorial, you’ll also want to delete the underlying docker containers for the marbles private data chaincode. Let’s run the following commands to clean up previous environments:
```
docker rm -f $(docker ps -a | awk '($2 ~ /dev-peer.*.marblesp.*/) {print $1}')
docker rmi -f $(docker images | awk '($1 ~ /dev-peer.*.marblesp.*/) {print $3}')
```
Start up the BYFN network with CouchDB by running the following command:
```
./byfn.sh up -c mychannel -s couchdb
```
This will create a simple Fabric network consisting of a single channel named `mychannel` with two organizations (each maintaining two peer nodes) and an ordering service while using CouchDB as the state database. Either LevelDB or CouchDB may be used with collections. CouchDB was chosen to demonstrate how to use indexes with private data.

> For collections to work, it is important to have cross organizational
> gossip configured correctly. Refer to our documentation on [Gossip
> data dissemination
> protocol](https://hyperledger-fabric.readthedocs.io/en/release-1.4/gossip.html),
> paying particular attention to the section on “anchor peers”. Our
> tutorial does not focus on gossip given it is already configured in
> the BYFN sample, but when configuring a channel, the gossip anchors
> peers are critical to configure for collections to work properly.

## Install and instantiate chaincode with a collection
Client applications interact with the blockchain ledger through chaincode. As such we need to install and instantiate the chaincode on every peer that will execute and endorse our transactions. Chaincode is installed onto a peer and then instantiated onto the channel using peer-commands.

### Install chaincode on all peers
As discussed above, the BYFN network includes two organizations, Org1 and Org2, with two peers each. Therefore the chaincode has to be installed on four peers:

-   peer0.org1.example.com
-   peer1.org1.example.com
-   peer0.org2.example.com
 -   peer1.org2.example.com

Assuming you have started the BYFN network, enter the CLI container:
```
docker exec -it cli bash
```

 1. Install the Marbles chaincode from the git repository onto the peer `peer0.org1.example.com`:
     ```
     peer chaincode install -n marblesp -v 1.0 -p github.com/chaincode/marbles02_private/go/
    ```

2. Use the CLI to switch the active peer to the second peer in Org1 and install the chaincode. Copy and paste the following entire block of commands into the CLI container and run them:
```
    export CORE_PEER_ADDRESS=peer1.org1.example.com:7051
    peer chaincode install -n marblesp -v 1.0 -p github.com/chaincode/marbles02_private/go/
```

3. Use the CLI to switch to Org2. Copy and paste the following block of commands as a group into the peer container and run them all at once:
```
   export CORE_PEER_LOCALMSPID=Org2MSP
   export PEER0_ORG2_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
   export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_ORG2_CA
   export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
```

4. Switch the active peer to the first peer in Org2 and install the chaincode:
```
    export CORE_PEER_ADDRESS=peer0.org2.example.com:7051
    peer chaincode install -n marblesp -v 1.0 -p github.com/chaincode/marbles02_private/go/
```
5. Switch the active peer to the second peer in org2 and install the chaincode:
```
     export CORE_PEER_ADDRESS=peer1.org2.example.com:7051
     peer chaincode install -n marblesp -v 1.0 -p github.com/chaincode/marbles02_private/go/
```

### Instantiate the chaincode on the channel
To configure the chaincode collections on the channel, specify the flag `--collections-config` along with the name of the collections JSON file, `collections_config.json`in our example.

Run the following commands to instantiate the marbles private data chaincode on the BYFN channel `mychannel`.

```
export ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
peer chaincode instantiate -o orderer.example.com:7050 --tls --cafile $ORDERER_CA -C mychannel -n marblesp -v 1.0 -c '{"Args":["init"]}' -P "OR('Org1MSP.member','Org2MSP.member')" --collections-config  $GOPATH/src/github.com/chaincode/marbles02_private/collections_config.json
```

> When specifying the value of the `--collections-config` flag, you will
> need to specify the fully qualified path to the
> collections_config.json file. For example: `--collections-config
> $GOPATH/src/github.com/chaincode/marbles02_private/collections_config.json`


## Store private data
Acting as a member of Org1, who is authorized to transact with all of the private data in the marbles private data sample, switch back to an Org1 peer and submit a request to add a marble:
```
export CORE_PEER_ADDRESS=peer0.org1.example.com:7051
export CORE_PEER_LOCALMSPID=Org1MSP
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
export PEER0_ORG1_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
```

Invoke the marbles `initMarble` function which creates a marble with private data — name `marble1` owned by `tom` with a color `blue`, size `35` and price of `99`. Note that the private data is passed using the `--transient` flag. Inputs passed as transient data will not be persisted in the transaction in order to keep the data private. Transient data is passed as binary data and therefore when using CLI it must be base64 encoded. We use an environment variable to capture the base64 encoded value.
```
export MARBLE=$(echo -n "{\"name\":\"marble1\",\"color\":\"blue\",\"size\":35,\"owner\":\"tom\",\"price\":99}" | base64 -w 0 )
peer chaincode invoke -o orderer.example.com:7050 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n marblesp -c '{"Args":["initMarble"]}'  --transient "{\"marble\":\"$MARBLE\"}"
```

## Query the private data as an authorized peer
Our collection definition allows all members of Org1 and Org2 to have the `name,  color,  size,  owner` private data in their side database, but only peers in Org1 can have the `price` private data in their side database. As an authorized peer in Org1, we will query both sets of private data.

The first `query` command calls the `readMarble` function which passes `collectionMarbles` as an argument:

```
peer chaincode query -C mychannel -n marblesp -c '{"Args":["readMarble","marble1"]}'
```

The second `query` command calls the `readMarblePrivateDetails` function which passes `collectionMarblePrivateDetails` as an argument:
```
peer chaincode query -C mychannel -n marblesp -c '{"Args":["readMarblePrivateDetails","marble1"]}'
```

## Query the private data as an unauthorized peer
Now we will switch to a member of Org2 which has the marbles private data`name,  color,  size,  owner` in its side database, but does not have the marbles `price` private data in its side database. We will query for both sets of private data.

### Switch to a peer in Org2
```
export CORE_PEER_ADDRESS=peer0.org2.example.com:7051
export CORE_PEER_LOCALMSPID=Org2MSP
export PEER0_ORG2_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_ORG2_CA
export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
```

### Query private data Org2 is authorized to
Peers in Org2 should have the first set of marbles private data (`name,  color,  size  and  owner`) in their side database and can access it using the `readMarble()` function which is called with the `collectionMarbles` argument.
```
peer chaincode query -C mychannel -n marblesp -c '{"Args":["readMarble","marble1"]}'
```
### Query private data Org2 is not authorized to
Peers in Org2 do not have the marbles `price` private data in their side database. When they try to query for this data, they get back a hash of the key matching the public state but will not have the private state.
```
peer chaincode query -C mychannel -n marblesp -c '{"Args":["readMarblePrivateDetails","marble1"]}'
```
## Purge Private Data
For use cases where private data only needs to be on the ledger until it can be replicated into an off-chain database, it is possible to “purge” the data after a certain set number of blocks, leaving behind only hash of the data that serves as immutable evidence of the transaction. Thus, it has a limited lifespan, and can be purged after existing unchanged on the blockchain for a designated number of blocks using the `blockToLive` property in the collection definition.

We will step through adding blocks to the chain, and then watch the price information get purged by issuing four new transactions (Create a new marble, followed by three marble transfers) which adds four new blocks to the chain. After the fourth transaction (third marble transfer), we will verify that the price private data is purged.

**Switch back to peer0 in Org1 using the following commands. Copy and paste the following code block and run it inside your peer container:**
```
export CORE_PEER_ADDRESS=peer0.org1.example.com:7051
export CORE_PEER_LOCALMSPID=Org1MSP
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
export PEER0_ORG1_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
```
Open a new terminal window and view the private data logs for this peer by running the following command:
```
docker logs peer0.org1.example.com 2>&1 | grep -i -a -E 'private|pvt|privdata'
```
Back in the cli container, query for the **marble1** price data by running the following command. (A Query does not create a new transaction on the ledger since no data is transacted).
```
peer chaincode query -C mychannel -n marblesp -c '{"Args":["readMarblePrivateDetails","marble1"]}'
```
Create a new **marble2** by issuing the following command. This transaction creates a new block on the chain.
```
export MARBLE=$(echo -n "{\"name\":\"marble2\",\"color\":\"blue\",\"size\":35,\"owner\":\"tom\",\"price\":99}" | base64 -w 0)
peer chaincode invoke -o orderer.example.com:7050 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n marblesp -c '{"Args":["initMarble"]}' --transient "{\"marble\":\"$MARBLE\"}"
```
Switch back to the Terminal window and view the private data logs for this peer again. You should see the block height increase by 1.

Transfer marble2 to “joe” by running the following command. This transaction will add a second new block on the chain.
```
export MARBLE_OWNER=$(echo -n "{\"name\":\"marble2\",\"owner\":\"joe\"}" | base64 -w 0)
peer chaincode invoke -o orderer.example.com:7050 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n marblesp -c '{"Args":["transferMarble"]}' --transient "{\"marble_owner\":\"$MARBLE_OWNER\"}"
```
Transfer marble2 to “tom” by running the following command. This transaction will create a third new block on the chain.

```
export MARBLE_OWNER=$(echo -n "{\"name\":\"marble2\",\"owner\":\"tom\"}" | base64 -w 0)
peer chaincode invoke -o orderer.example.com:7050 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n marblesp -c '{"Args":["transferMarble"]}' --transient "{\"marble_owner\":\"$MARBLE_OWNER\"}"
```
Finally, transfer marble2 to “jerry” by running the following command. This transaction will create a fourth new block on the chain. The `price` private data should be purged after this transaction.
```
export MARBLE_OWNER=$(echo -n "{\"name\":\"marble2\",\"owner\":\"jerry\"}" | base64 -w 0)
peer chaincode invoke -o orderer.example.com:7050 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n marblesp -c '{"Args":["transferMarble"]}' --transient "{\"marble_owner\":\"$MARBLE_OWNER\"}"
```

Back in the peer container, query for the marble1 price data by running the following command:
```
peer chaincode query -C mychannel -n marblesp -c '{"Args":["readMarblePrivateDetails","marble1"]}'
```
Because the price data has been purged, you should no longer be able to see it. You should see something similar to:
```
Error: endorsement failure during query. response: status:500
message:"{\"Error\":\"Marble private details does not exist: marble1\"}"
```

## Using indexes with private data
Indexes can also be applied to private data collections, by packaging indexes in the  `META-INF/statedb/couchdb/collections/<collection_name>/indexes`  directory alongside the chaincode. An example index is available  [here](https://github.com/hyperledger/fabric-samples/blob/master/chaincode/marbles02_private/go/META-INF/statedb/couchdb/collections/collectionMarbles/indexes/indexOwner.json)  .

For deployment of chaincode to production environments, it is recommended to define any indexes alongside chaincode so that the chaincode and supporting indexes are deployed automatically as a unit, once the chaincode has been installed on a peer and instantiated on a channel. The associated indexes are automatically deployed upon chaincode instantiation on the channel when the  `--collections-config`  flag is specified pointing to the location of the collection JSON file.

## Additional resources
For additional private data education, a video tutorial has been created: https://youtu.be/qyjDi93URJE

