# System Chaincode Plugins

System chaincodes are specialized chaincodes that run as part of the peer process as opposed to user chaincodes that run in separate docker containers. As such they have more access to resources in the peer and can be used for implementing features that are difficult or impossible to be implemented through user chaincodes. Examples of System Chaincodes include QSCC (Query System Chaincode) for ledger and other Fabric-related queries, CSCC (Configuration System Chaincode) which helps regulate access control, and LSCC (Lifecycle System Chaincode).

Unlike a user chaincode, a system chaincode is not installed and instantiated using proposals from SDKs or CLI. It is registered and deployed by the peer at start-up.

System chaincodes can be linked to a peer in two ways: statically, and dynamically using Go plugins. This tutorial will outline how to develop and load system chaincodes as plugins.

## Developing Plugins
A system chaincode is a program written in [Go](https://golang.org/) and loaded using the Go [plugin](https://golang.org/pkg/plugin) package.

## Configuring Plugins
Plugins are configured in the `chaincode.systemPlugin` section in `core.yaml`:
```
chaincode:
  systemPlugins:
    - enabled: true
      name: mysyscc
      path: /opt/lib/syscc.so
      invokableExternal: true
      invokableCC2CC: true
```      

A system chaincode must also be whitelisted in the `chaincode.system` section in `core.yaml`:
```
chaincode:
  system:
    mysyscc: enable
```
