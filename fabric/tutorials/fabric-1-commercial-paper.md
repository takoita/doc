# Commercial paper tutorial

## Déployer le réseau blockchain
```
cd fabric-samples/basic-network
./start.sh
```

## Actions dans l'organisation MagnetoCorp
### Aggréger les logs de tous les conteneurs pour faciliter leur lecture:
```
cd commercial-paper/organization/magnetocorp/configuration/cli/
./monitordocker.sh net_basic
# ou si le port 8000 est occupé: ./monitordocker.sh net_basic<port>
```
### Démarrer le conteneur utilitaire de Fabric: hyperledger/fabric-tools
```
cd commercial-paper/organization/magnetocorp/configuration/cli/

docker-compose -f docker-compose.yml up -d cliMagnetoCorp
```
### Installer le smart contract
```
docker exec cliMagnetoCorp peer chaincode install -n papercontract -v 0 -p /opt/gopath/src/github.com/contract -l node
```

### Instantier le contrat
```
docker exec cliMagnetoCorp peer chaincode instantiate -n papercontract -v 0 -l node -c '{"Args":["org.papernet.commercialpaper:instantiate"]}' -C mychannel -P "AND ('Org1MSP.member')"
```

### Installer l'application javascript
```
cd commercial-paper/organization/magnetocorp/application

npm install
```

### Création du wallet
```
cd commercial-paper/organization/magnetocorp/application

node addToWallet.js
```

### Application issue
```
node issue.js
```

## Actions dans l'organisation DigiBank
### Lancement du conteneur docker utilitaire au sein de l'organisation DigiBank
```
cd commercial-paper/organization/digibank/configuration/cli/

docker-compose -f docker-compose.yml up -d cliDigiBank
```

### Installer l'application de DigiBank et créer le wallet
```
cd commercial-paper/organization/digibank/application

npm install

node addToWallet.js
```

### Application Buy
```
node buy.js
```

### Application Redeem
```
node redeem.js
```