# Using CouchDB
This tutorial will describe the steps required to use the CouchDB as the state database with Hyperledger Fabric. By now, you should be familiar with Fabric concepts and have explored some of the samples and tutorials.

## Why CouchDB?
Fabric supports two types of peer databases. LevelDB is the default state database embedded in the peer node and stores chaincode data as simple key-value pairs and supports key, key range, and composite key queries only. CouchDB is an optional alternate state database that supports rich queries when chaincode data values are modeled as JSON. Rich queries are more flexible and efficient against large indexed data stores, when you want to query the actual data value content rather than the keys. CouchDB is a JSON document datastore rather than a pure key-value store therefore enabling indexing of the contents of the documents in the database.

In order to leverage the benefits of CouchDB, namely content-based JSON queries,your data must be modeled in JSON format. You must decide whether to use LevelDB or CouchDB before setting up your network. Switching a peer from using LevelDB to CouchDB is not supported due to data compatibility issues. All peers on the network must use the same database type. If you have a mix of JSON and binary data values, you can still use CouchDB, however the binary values can only be queried based on key, key range, and composite key queries.

## Enable CouchDB in Hyperledger Fabric
CouchDB runs as a separate database process alongside the peer, therefore there are additional considerations in terms of setup, management, and operations. A docker image of CouchDB is available and we recommend that it be run on the same server as the peer. You will need to setup one CouchDB container per peer and update each peer container by changing the configuration found in core.yaml to point to the CouchDB container. The `core.yaml` file must be located in the directory specified by the environment variable FABRIC_CFG_PATH:
-   For docker deployments,  `core.yaml`  is pre-configured and located in the peer container  `FABRIC_CFG_PATH`  folder. However when using docker environments, you typically pass environment variables by editing the  `docker-compose-couch.yaml`  to override the core.yaml
-   For native binary deployments,  `core.yaml`  is included with the release artifact distribution.

Edit the `stateDatabase` section of `core.yaml`. Specify `CouchDB` as the `stateDatabase` and fill in the associated `couchDBConfig` properties. For more details on configuring CouchDB to work with fabric, refer [here](http://hyperledger-fabric.readthedocs.io/en/master/couchdb_as_state_database.html#couchdb-configuration). To view an example of a core.yaml file configured for CouchDB, examine the BYFN `docker-compose-couch.yaml` in the `HyperLedger/fabric-samples/first-network` directory.

## Create an index
Indexes allow a database to be queried without having to examine every row with every query, making them run faster and more efficiently. Normally, indexes are built for frequently occurring query criteria allowing the data to be queried more efficiently. To leverage the major benefit of CouchDB – the ability to perform rich queries against JSON data – indexes are not required, but they are strongly recommended for performance. Also, if sorting is required in a query, CouchDB requires an index of the sorted fields.
> Rich queries that do not have an index will work but may throw a
> warning in the CouchDB log that the index was not found. However, if a
> rich query includes a sort specification, then an index on that field
> is required; otherwise, the query will fail and an error will be
> thrown.

To demonstrate building an index, we will use the data from the [Marbles sample](https://github.com/hyperledger/fabric-samples/blob/master/chaincode/marbles02/go/marbles_chaincode.go). In this example, the Marbles data structure is defined as:
```
type marble struct {
         ObjectType string `json:"docType"` //docType is used to distinguish the various types of objects in state database
         Name       string `json:"name"`    //the field tags are needed to keep case from bouncing around
         Color      string `json:"color"`
         Size       int    `json:"size"`
         Owner      string `json:"owner"`
}
```
The attribute docType is a pattern used in the chaincode to differentiate different data types that may need to be queried separately. When using CouchDB, it recommended to include this docType attribute to distinguish each type of document in the chaincode namespace.

When defining an index for use in chaincode queries, each one must be defined in its own text file with the extension *.json and the index definition must be formatted in the CouchDB index JSON format.

To define an index, three pieces of information are required:
 -   fields: these are the frequently queried fields
 -   name: name of the index
 -   type: always json in this context
 
 For example, a simple index named foo-index for a field named foo:
 ```
{
    "index": {
        "fields": ["foo"]
    },
    "name" : "foo-index",
    "type" : "json"
} 
 ```
 
 
Optionally the design document attribute `ddoc` can be specified on the index definition. A [design document](http://guide.couchdb.org/draft/design.html) is CouchDB construct designed to contain indexes. Indexes can be grouped into design documents for efficiency but CouchDB recommends one index per design document.

When defining an index it is a good practice to include the `ddoc` attribute and value along with the index name. It is important to include this attribute to ensure that you can update the index later if needed. Also it gives you the ability to explicitly specify which index to use on a query.

Here is another example of an index definition from the Marbles sample with the index name `indexOwner` using multiple fields `docType` and `owner` and includes the `ddoc` attribute:
```
{
  "index":{
      "fields":["docType","owner"] // Names of the fields to be queried
  },
  "ddoc":"indexOwnerDoc", // (optional) Name of the design document in which the index will be created.
  "name":"indexOwner",
  "type":"json"
}
```
 
In the example above, if the design document indexOwnerDoc does not already exist, it is automatically created when the index is deployed.

In general, you should model index fields to match the fields that will be used in query filters and sorts. For more details on building an index in JSON format refer to the [CouchDB documentation](http://docs.couchdb.org/en/latest/api/database/find.html#db-index).

A final word on indexing, Fabric takes care of indexing the documents in the database using a pattern called `index  warming`. CouchDB does not typically index new or updated documents until the next query. Fabric ensures that indexes stay ‘warm’ by requesting an index update after every block of data is committed. This ensures queries are fast because they do not have to index documents before running the query. This process keeps the index current and refreshed every time new records are added to the state database.

## Add the index to your chaincode folder
If your chaincode installation and instantiation uses the Hyperledger Fabric Node SDK, the JSON index files can be located in any folder as long as it conforms to this  [directory structure](https://fabric-sdk-node.github.io/tutorial-metadata-chaincode.html). During the chaincode installation using the client.installChaincode() API, include the attribute (`metadataPath`) in the  [installation request](https://fabric-sdk-node.github.io/global.html#ChaincodeInstallRequest). The value of the metadataPath is a string representing the absolute path to the directory structure containing the JSON index file(s).

Alternatively, if you are using the  peer-commands  to install and instantiate the chaincode, then the JSON index files must be located under the path  `META-INF/statedb/couchdb/indexes`  which is located inside the directory where the chaincode resides.

### Start the network
Now start up the BYFN network with CouchDB by running the following command:
```
./byfn.sh up -c mychannel -s couchdb
```

## Install and instantiate the Chaincode
Assuming you have started the BYFN network, navigate into the CLI container using the command:
```
docker exec -it cli bash

peer chaincode install -n marbles -v 1.0 -p github.com/chaincode/marbles02/go

export CHANNEL_NAME=mychannel

peer chaincode instantiate -o orderer.example.com:7050 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C $CHANNEL_NAME -n marbles -v 1.0 -c '{"Args":["init"]}' -P "OR ('Org0MSP.peer','Org1MSP.peer')"
```

### Verify index was deployed
Indexes will be deployed to each peer’s CouchDB state database once the chaincode is both installed on the peer and instantiated on the channel. You can verify that the CouchDB index was created successfully by examining the peer log in the Docker container:
```
docker logs peer0.org1.example.com  2>&1 | grep "CouchDB index"
```

## Query the CouchDB State Database
Specifying an index name on a query is optional. If not specified, and an index already exists for the fields being queried, the existing index will be automatically used.

> It is a good practice to explicitly include an index name on a query
> using the `use_index`keyword. Without it, CouchDB may pick a less
> optimal index. Also CouchDB may not use an index at all and you may
> not realize it, at the low volumes during testing. Only upon higher
> volumes you may realize slow performance because CouchDB is not using
> an index and you assumed it was.

### Build the query in chaincode
You can perform complex rich queries against the chaincode data values using the CouchDB JSON query language within chaincode. As we explored above, the [marbles02 sample chaincode](https://github.com/hyperledger/fabric-samples/blob/master/chaincode/marbles02/go/marbles_chaincode.go) includes an index and rich queries are defined in the functions - `queryMarbles` and `queryMarblesByOwner`:

### Run the query using the peer command
In absence of a client application to test rich queries defined in chaincode, peer commands can be used. Peer commands run from the command line inside the docker container. We will customize the [peer chaincode query](http://hyperledger-fabric.readthedocs.io/en/master/commands/peerchaincode.html?%20chaincode%20query#peer-chaincode-query) command to use the Marbles index `indexOwner` and query for all marbles owned by “tom” using the `queryMarbles` function.

Before querying the database, we should add some data. Run the following command in the peer container to create a marble owned by “tom”:
```
peer chaincode invoke -o orderer.example.com:7050 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C $CHANNEL_NAME -n marbles -c '{"Args":["initMarble","marble1","blue","35","tom"]}'
```
After an index has been deployed during chaincode instantiation, it will automatically be utilized by chaincode queries. CouchDB can determine which index to use based on the fields being queried. If an index exists for the query criteria it will be used. However the recommended approach is to specify the `use_index` keyword on the query:
```
// Rich Query with index name explicitly specified:
peer chaincode query -C $CHANNEL_NAME -n marbles -c '{"Args":["queryMarbles", "{\"selector\":{\"docType\":\"marble\",\"owner\":\"tom\"}, \"use_index\":[\"_design/indexOwnerDoc\", \"indexOwner\"]}"]}'
```
With CouchDB, if you plan to explicitly include the index name on the query, then the index definition must include the `ddoc` value, so it can be referenced with the `use_index` keyword.

### Query the CouchDB State Database With Pagination
When large result sets are returned by CouchDB queries, a set of APIs is available which can be called by chaincode to paginate the list of results. Pagination provides a mechanism to partition the result set by specifying a `pagesize` and a start point – a `bookmark` which indicates where to begin the result set.The client application iteratively invokes the chaincode that executes the query until no more results are returned. For more information refer to this [topic on pagination with CouchDB](http://hyperledger-fabric.readthedocs.io/en/master/couchdb_as_state_database.html#couchdb-pagination).

We will use the [Marbles sample](https://github.com/hyperledger/fabric-samples/blob/master/chaincode/marbles02/go/marbles_chaincode.go) function `queryMarblesWithPagination` to demonstrate how pagination can be implemented in chaincode and the client application.

In order to demonstrate pagination, more data is required:
```
peer chaincode invoke -o orderer.example.com:7050 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C $CHANNEL_NAME -n marbles -c '{"Args":["initMarble","marble2","yellow","35","tom"]}'
peer chaincode invoke -o orderer.example.com:7050 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C $CHANNEL_NAME -n marbles -c '{"Args":["initMarble","marble3","green","20","tom"]}'
peer chaincode invoke -o orderer.example.com:7050 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C $CHANNEL_NAME -n marbles -c '{"Args":["initMarble","marble4","purple","20","tom"]}'
peer chaincode invoke -o orderer.example.com:7050 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C $CHANNEL_NAME -n marbles -c '{"Args":["initMarble","marble5","blue","40","tom"]}'
```

In addition to the arguments for the query in the previous example, queryMarblesWithPagination adds pagesize and bookmark. PageSize specifies the number of records to return per query. The bookmark is an “anchor” telling couchDB where to begin the page. (Each page of results returns a unique bookmark.)

The following example is a peer command which calls queryMarblesWithPagination with a pageSize of 3 and no bookmark specified.
> When no bookmark is specified, the query starts with the “first” page
> of records.

```
// Rich Query with index name explicitly specified and a page size of 3:
peer chaincode query -C $CHANNEL_NAME -n marbles -c '{"Args":["queryMarblesWithPagination", "{\"selector\":{\"docType\":\"marble\",\"owner\":\"tom\"}, \"use_index\":[\"_design/indexOwnerDoc\", \"indexOwner\"]}","3",""]}'
```

> Bookmarks are uniquely generated by CouchDB for each query and
> represent a placeholder in the result set. Pass the returned bookmark
> on the subsequent iteration of the query to retrieve the next set of
> results.

The following is a peer command to call queryMarblesWithPagination with a pageSize of `3`. Notice this time, the query includes the bookmark returned from the previous query:
```
peer chaincode query -C $CHANNEL_NAME -n marbles -c '{"Args":["queryMarblesWithPagination", "{\"selector\":{\"docType\":\"marble\",\"owner\":\"tom\"}, \"use_index\":[\"_design/indexOwnerDoc\", \"indexOwner\"]}","3","g1AAAABLeJzLYWBgYMpgSmHgKy5JLCrJTq2MT8lPzkzJBYqz5yYWJeWkGoOkOWDSOSANIFk2iCyIyVySn5uVBQAGEhRz"]}'
```

The final command is a peer command to call queryMarblesWithPagination with a pageSize of 3 and with the bookmark from the previous query:
```
peer chaincode query -C $CHANNEL_NAME -n marbles -c '{"Args":["queryMarblesWithPagination", "{\"selector\":{\"docType\":\"marble\",\"owner\":\"tom\"}, \"use_index\":[\"_design/indexOwnerDoc\", \"indexOwner\"]}","3", "g1AAAABLeJzLYWBgYMpgSmHgKy5JLCrJTq2MT8lPzkzJBYqz5yYWJeWkmoKkOWDSOSANIFk2iCyIyVySn5uVBQAGYhR1"]}'
```

For an example of how a client application can iterate over the result sets using pagination, search for the `getQueryResultForQueryStringWithPagination` function in the [Marbles sample](https://github.com/hyperledger/fabric-samples/blob/master/chaincode/marbles02/go/marbles_chaincode.go).

## Update an Index
It may be necessary to update an index over time. The same index may exist in subsequent versions of the chaincode that gets installed. In order for an index to be updated, the original index definition must have included the design document ddoc attribute and an index name. To update an index definition, use the same index name but alter the index definition. Simply edit the index JSON file and add or remove fields from the index. Fabric only supports the index type JSON, changing the index type is not supported. The updated index definition gets redeployed to the peer’s state database when the chaincode is installed and instantiated. Changes to the index name or ddoc attributes will result in a new index being created and the original index remains unchanged in CouchDB until it is removed.

> If the state database has a significant volume of data, it will take
> some time for the index to be re-built, during which time chaincode
> invokes that issue queries may fail or timeout.

### Iterating on your index definition

If you have access to your peer’s CouchDB state database in a development environment, you can iteratively test various indexes in support of your chaincode queries. Any changes to chaincode though would require redeployment. Use the [CouchDB Fauxton interface](http://docs.couchdb.org/en/latest/fauxton/index.html) or a command line curl utility to create and update indexes.

> The Fauxton interface is a web UI for the creation, update, and
> deployment of indexes to CouchDB. If you want to try out this
> interface, there is an example of the format of the Fauxton version of
> the index in Marbles sample. If you have deployed the BYFN network
> with CouchDB, the Fauxton interface can be loaded by opening a browser
> and navigating to `http://localhost:5984/_utils`.

Alternatively, if you prefer not use the Fauxton UI, the following is an example of a curl command which can be used to create the index on the database `mychannel_marbles`:
```
curl -i -X POST -H "Content-Type: application/json" -d
       "{\"index\":{\"fields\":[\"docType\",\"owner\"]},
         \"name\":\"indexOwner\",
         \"ddoc\":\"indexOwnerDoc\",
         \"type\":\"json\"}" http://hostname:port/mychannel_marbles/_index
```
If you are using BYFN configured with CouchDB, replace hostname:port with localhost:5984.

## Delete an Index
Index deletion is not managed by Fabric tooling. If you need to delete an index, manually issue a curl command against the database or delete it using the Fauxton interface.

The format of the curl command to delete an index would be:
```
curl -X DELETE http://localhost:5984/{database_name}/_index/{design_doc}/json/{index_name} -H  "accept: */*" -H  "Host: localhost:5984"
```
To delete the index used in this tutorial, the curl command would be:
```
curl -X DELETE http://localhost:5984/mychannel_marbles/_index/indexOwnerDoc/json/indexOwner -H  "accept: */*" -H  "Host: localhost:5984"
```






