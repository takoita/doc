https://hyperledger-fabric.readthedocs.io/en/release-1.3/build_network.html#start-the-network
# Generate bootstrap artifacts and start the network
```shell
cryptogen generate --config=./crypto-config.yaml

export CHANNEL_NAME="mychannel" 

export FABRIC_CFG_PATH=$PWD # permet � l'outil configtxgen de savoir o� trouver le fichier configtx.xml
configtxgen -profile TwoOrgsOrdererGenesis -outputBlock ./channel-artifacts/genesis.block

configtxgen -profile TwoOrgsChannel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID $CHANNEL_NAME

configtxgen -profile TwoOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/Org1MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org1MSP

configtxgen -profile TwoOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/Org2MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org2MSP

export COMPOSE_PROJECT_NAME="net"

export IMAGE_TAG="1.3.0"

docker-compose -f docker-compose-cli.yaml up -d
```
# Create & Join Channel
## Environment variables
```shell
export ORDERER_ADDRESS="orderer.aboubakarkoita.com:7050"
export ORDERER_TLS_ROOT_CA="/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/aboubakarkoita.com/orderers/orderer.aboubakarkoita.com/msp/tlscacerts/tlsca.aboubakarkoita.com-cert.pem"

export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.aboubakarkoita.com/users/Admin@org1.aboubakarkoita.com/msp
export CORE_PEER_ADDRESS=peer0.org1.aboubakarkoita.com:7051
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.aboubakarkoita.com/peers/peer0.org1.aboubakarkoita.com/tls/ca.crt

export CHANNEL_NAME=mychannel
```
##Create the channel
```shell
# the channel.tx file is mounted in the channel-artifacts directory within your CLI container
# as a result, we pass the full path for the file
# we also pass the path for the orderer ca-cert in order to verify the TLS handshake
# be sure to export or replace the $CHANNEL_NAME variable appropriately

peer channel create -o $ORDERER_ADDRESS  -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx --tls --cafile $ORDERER_TLS_ROOT_CA


# By default, this joins ``peer0.org1.example.com`` only
# the <channel-ID.block> was returned by the previous command
# if you have not modified the channel name, you will join with mychannel.block
# if you have created a different channel name, then pass in the appropriately named block

peer channel join -b mychannel.block

export CORE_PEER_ADDRESS=peer1.org1.aboubakarkoita.com:7051
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.aboubakarkoita.com/peers/peer1.org1.aboubakarkoita.com/tls/ca.crt

peer channel join -b mychannel.block


export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.aboubakarkoita.com/users/Admin@org2.aboubakarkoita.com/msp
export CORE_PEER_ADDRESS=peer0.org2.aboubakarkoita.com:7051
export CORE_PEER_LOCALMSPID="Org2MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.aboubakarkoita.com/peers/peer0.org2.aboubakarkoita.com/tls/ca.crt

peer channel join -b mychannel.block


export CORE_PEER_ADDRESS=peer1.org2.aboubakarkoita.com:7051
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.aboubakarkoita.com/peers/peer1.org2.aboubakarkoita.com/tls/ca.crt

peer channel join -b mychannel.block
```

##Update the anchor peers
```
export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.aboubakarkoita.com/users/Admin@org1.aboubakarkoita.com/msp
export CORE_PEER_ADDRESS=peer0.org1.aboubakarkoita.com:7051
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.aboubakarkoita.com/peers/peer0.org1.aboubakarkoita.com/tls/ca.crt

peer channel update -o $ORDERER_ADDRESS -c $CHANNEL_NAME -f ./channel-artifacts/Org1MSPanchors.tx --tls --cafile $ORDERER_TLS_ROOT_CA


export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.aboubakarkoita.com/users/Admin@org2.aboubakarkoita.com/msp
export CORE_PEER_ADDRESS=peer0.org2.aboubakarkoita.com:7051
export CORE_PEER_LOCALMSPID="Org2MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.aboubakarkoita.com/peers/peer0.org2.aboubakarkoita.com/tls/ca.crt

peer channel update -o $ORDERER_ADDRESS -c $CHANNEL_NAME -f ./channel-artifacts/Org2MSPanchors.tx --tls --cafile $ORDERER_TLS_ROOT_CA
```

#Install & Instantiate Chaincode
##Install
```
peer chaincode install -n firstcc -v 2.0 -l java -p /opt/gopath/src/github.com/chaincode/example


export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.aboubakarkoita.com/users/Admin@org2.aboubakarkoita.com/msp

export CORE_PEER_ADDRESS=peer0.org2.aboubakarkoita.com:7051

export CORE_PEER_LOCALMSPID="Org2MSP"

export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.aboubakarkoita.com/peers/peer0.org2.aboubakarkoita.com/tls/ca.crt

peer chaincode install -n firstcc -v 2.0 -l java -p /opt/gopath/src/github.com/chaincode/example
```

##Instantiate and query
```
peer chaincode instantiate -o orderer.aboubakarkoita.com:7050 --tls --cafile $ORDERER_TLS_ROOT_CA -C $CHANNEL_NAME -n firstcc -l java -v 2.0 -c '{"Args":["init","a", "100", "b","200"]}' -P "AND ('Org1MSP.peer','Org2MSP.peer')"

#Query
peer chaincode query -C $CHANNEL_NAME -n firstcc -c '{"Args":["query","a"]}'
```

##Invoke and query
```
export PEER0_ORG1_TLS_ROOT_CA="/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.aboubakarkoita.com/peers/peer0.org1.aboubakarkoita.com/tls/ca.crt"
export PEER0_ORG2_TLS_ROOT_CA="/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.aboubakarkoita.com/peers/peer0.org2.aboubakarkoita.com/tls/ca.crt"

peer chaincode invoke -o $ORDERER_ADDRESS --tls true --cafile $ORDERER_TLS_ROOT_CA -C $CHANNEL_NAME -n firstcc  --peerAddresses peer0.org1.aboubakarkoita.com:7051 --tlsRootCertFiles $PEER0_ORG1_TLS_ROOT_CA --peerAddresses peer0.org2.aboubakarkoita.com:7051 --tlsRootCertFiles $PEER0_ORG2_TLS_ROOT_CA  -c '{"Args":["invoke","a","b","10"]}'

#Query to check
peer chaincode query -C $CHANNEL_NAME -n firstcc -c '{"Args":["query","a"]}'
```
