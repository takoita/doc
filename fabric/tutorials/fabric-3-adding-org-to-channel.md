#Adding an Org to a Channel
## Setup the Environment
```
./byfn.sh down

./byfn.sh generate

./byfn.sh up
```

## Bring Org3 into the Channel with the Script
```
./eyfn.sh up
```

## Bring Org3 into the Channel Manually
If you’ve used the eyfn.sh script, you’ll need to bring your network down:
```
./eyfn.sh down
```

When the network is down, bring it back up again:
```
./byfn.sh generate
./byfn.sh up
```

### Generate the Org3 Crypto Material
```
cd org3-artifacts

cryptogen generate --config=./org3-crypto.yaml

# Use the configtxgen utility to print out the Org3-specific configuration material in JSON. 
export FABRIC_CFG_PATH=$PWD
configtxgen -printOrg Org3MSP > ../channel-artifacts/org3.json

#Our final piece of housekeeping is to port the Orderer Org’s MSP material into the Org3 crypto-config directory.
#In particular, we are concerned with the Orderer’s TLS root cert, which will allow for secure communication between Org3 entities and the network’s ordering node.
cd ../ && cp -r crypto-config/ordererOrganizations org3-artifacts/crypto-config/
```
### Prepare the CLI Environment
The update process makes use of the configuration translator tool – configtxlator. This tool provides a stateless REST API independent of the SDK.
Additionally it provides a CLI, to simplify configuration tasks in Fabric networks. The tool allows for the easy conversion between
different equivalent data representations/formats (in this case, between protobufs and JSON). Additionally, the tool can compute a configuration update 
transaction based on the differences between two channel configurations.

```
docker exec -it cli bash

#Export the ORDERER_CA and CHANNEL_NAME variables:
export ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
export CHANNEL_NAME=mychannel

#Check to make sure the variables have been properly set:
echo $ORDERER_CA && echo $CHANNEL_NAME
```

### Fetch the Configuration
Now we have a CLI container with our two key environment variables – ORDERER_CA and CHANNEL_NAME exported.
Let’s go fetch the most recent config block for the channel – mychannel.
```
peer channel fetch config config_block.pb -o orderer.example.com:7050 -c $CHANNEL_NAME --tls --cafile $ORDERER_CA
```

### Convert the Configuration to JSON and Trim It Down
Now we will make use of the configtxlator tool to decode this channel configuration block into JSON format (which can be read and modified by humans).
We also must strip away all of the headers, metadata, creator signatures, and so on that are irrelevant to the change we want to make.
We accomplish this by means of the jq tool:

```
configtxlator proto_decode --input config_block.pb --type common.Block | jq .data.data[0].payload.data.config > config.json
```

### Add the Org3 Crypto Material
We’ll use the jq tool once more to append the Org3 configuration definition – org3.json – to the channel’s application groups field, and name the output – modified_config.json.
```
jq -s '.[0] * {"channel_group":{"groups":{"Application":{"groups": {"Org3MSP":.[1]}}}}}' config.json ./channel-artifacts/org3.json > modified_config.json
```

First, translate config.json back into a protobuf called config.pb:
```
configtxlator proto_encode --input config.json --type common.Config --output config.pb

```

Next, encode modified_config.json to modified_config.pb:
```
configtxlator proto_encode --input modified_config.json --type common.Config --output modified_config.pb
```

Now use configtxlator to calculate the delta between these two config protobufs. This command will output a new protobuf binary named org3_update.pb:
```
configtxlator compute_update --channel_id $CHANNEL_NAME --original config.pb --updated modified_config.pb --output org3_update.pb
```

Before submitting the channel update, we need to perform a few final steps. First, let’s decode this object into editable JSON format and call it org3_update.json:
```
configtxlator proto_decode --input org3_update.pb --type common.ConfigUpdate | jq . > org3_update.json
```

Now, we have a decoded update file – org3_update.json – that we need to wrap in an envelope message. This step will give us back the header field that we stripped away earlier. We’ll name this file org3_update_in_envelope.json:
```
echo '{"payload":{"header":{"channel_header":{"channel_id":"mychannel", "type":2}},"data":{"config_update":'$(cat org3_update.json)'}}}' | jq . > org3_update_in_envelope.json
```

Using our properly formed JSON – org3_update_in_envelope.json – we will leverage the configtxlator tool one last time and convert it into the fully fledged protobuf format that Fabric requires. We’ll name our final update object org3_update_in_envelope.pb:
```
configtxlator proto_encode --input org3_update_in_envelope.json --type common.Envelope --output org3_update_in_envelope.pb
```

### Sign and Submit the Config Update
The modification policy (mod_policy) for our channel Application group is set to the default of “MAJORITY”, which means that we need a majority of existing org admins to sign it. Because we have only two orgs – Org1 and Org2 – and the majority of two is two, we need both of them to sign.
Without both signatures, the ordering service will reject the transaction for failing to fulfill the policy.

First, let’s sign this update proto as the Org1 Admin. Remember that the CLI container is bootstrapped with the Org1 MSP material, so we simply need to issue the peer channel signconfigtx command:
```
peer channel signconfigtx -f org3_update_in_envelope.pb
```

The final step is to switch the CLI container’s identity to reflect the Org2 Admin user. We do this by exporting four environment variables specific to the Org2 MSP.
>Switching between organizations to sign a config transaction (or to do anything else) is not reflective of a real-world Fabric operation. A single container would never be mounted with an entire network’s crypto material. 
> Rather, the config update would need to be securely passed out-of-band to an Org2 Admin for inspection and approval.

Export the Org2 environment variables:
```
# you can issue all of these commands at once

export CORE_PEER_LOCALMSPID="Org2MSP"

export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt

export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp

export CORE_PEER_ADDRESS=peer0.org2.example.com:7051
```

Lastly, we will issue the peer channel update command. The Org2 Admin signature will be attached to this call so there is no need to manually sign the protobuf a second time:
> The upcoming update call to the ordering service will undergo a series of systematic signature and policy checks.
>As such you may find it useful to stream and inspect the ordering node’s logs. 
>From another shell, issue a docker logs -f orderer.example.com command to display them.

```
peer channel update -f org3_update_in_envelope.pb -c $CHANNEL_NAME -o orderer.example.com:7050 --tls --cafile $ORDERER_CA
```
### Configuring Leader Election
Newly joining peers are bootstrapped with the genesis block, which does not contain information about the organization that is being added in the channel configuration update. Therefore new peers are not able to utilize gossip as they cannot verify blocks forwarded by other peers from their own organization until they get the configuration transaction which added the organization to the channel. Newly added peers must therefore have one of the following configurations so that they receive blocks from the ordering service:

1. To utilize static leader mode, configure the peer to be an organization leader:
```
CORE_PEER_GOSSIP_USELEADERELECTION=false
CORE_PEER_GOSSIP_ORGLEADER=true
```

2. To utilize dynamic leader election, configure the peer to use leader election:
```
CORE_PEER_GOSSIP_USELEADERELECTION=true
CORE_PEER_GOSSIP_ORGLEADER=false
```

### Join Org3 to the Channel
At this point, the channel configuration has been updated to include our new organization – Org3 –
meaning that peers attached to it can now join mychannel.

First, let’s launch the containers for the Org3 peers and an Org3-specific CLI.

```
docker-compose -f docker-compose-org3.yaml up -d
````

With the three new containers now running, exec into the Org3-specific CLI container:
```
docker exec -it Org3cli bash
```

Just as we did with the initial CLI container, export the two key environment variables: ORDERER_CA and CHANNEL_NAME:
```
export ORDERER_CA="/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem"
export CHANNEL_NAME="mychannel"
```
Check to make sure the variables have been properly set:
```
echo $ORDERER_CA && echo $CHANNEL_NAME
```

Now let’s send a call to the ordering service asking for the genesis block of mychannel.
The ordering service is able to verify the Org3 signature attached to this call as a result of our successful channel update. If Org3 has not been successfully appended to the channel config, the ordering service should reject this request.

```
peer channel fetch 0 mychannel.block -o orderer.example.com:7050 -c $CHANNEL_NAME --tls --cafile $ORDERER_CA
```

Notice, that we are passing a 0 to indicate that we want the first block on the channel’s ledger (i.e. the genesis block). If we simply passed the peer channel fetch config command, then we would have received block 5 – the updated config with Org3 defined.
However, we can’t begin our ledger with a downstream block – we must start with block 0.

Issue the peer channel join command and pass in the genesis block – mychannel.block:
```
peer channel join -b mychannel.block
```

To join the second peer for Org3, export the TLS and ADDRESS variables and reissue the peer channel join command:
```
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org3.example.com/peers/peer1.org3.example.com/tls/ca.crt
export CORE_PEER_ADDRESS=peer1.org3.example.com:7051

peer channel join -b mychannel.block
```

### Upgrade and Invoke Chaincode
The final piece of the puzzle is to increment the chaincode version and update the endorsement policy to include Org3. Since we know that an upgrade is coming, we can forgo the futile exercise of installing version 1 of the chaincode.
We are solely concerned with the new version where Org3 will be part of the endorsement policy, therefore we’ll jump directly to version 2 of the chaincode.

From the Org3 CLI:
```
peer chaincode install -n mycc -v 2.0 -p github.com/chaincode/chaincode_example02/go/
```

Install on peer0.org3:
```
export CORE_PEER_ADDRESS=peer0.org3.example.com:7051

peer chaincode install -n mycc -v 2.0 -p github.com/chaincode/chaincode_example02/go/
```

Now jump back to the original CLI container and install the new version on the Org1 and Org2 peers. We submitted the channel update call with the Org2 admin identity, so the container is still acting on behalf of peer0.org2:
```
peer chaincode install -n mycc -v 2.0 -p github.com/chaincode/chaincode_example02/go/
```

Flip to the peer0.org1 identity:
```
export CORE_PEER_LOCALMSPID="Org1MSP"

export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt

export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp

export CORE_PEER_ADDRESS=peer0.org1.example.com:7051
```

And install again:
```
peer chaincode install -n mycc -v 2.0 -p github.com/chaincode/chaincode_example02/go/
```

Now we’re ready to upgrade the chaincode. There have been no modifications to the underlying source code, we are simply adding Org3 to the endorsement policy for a chaincode – mycc – on mychannel.

Send the call:
```
peer chaincode upgrade -o orderer.example.com:7050 --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C $CHANNEL_NAME -n mycc -v 2.0 -c '{"Args":["init","a","90","b","210"]}' -P "OR ('Org1MSP.peer','Org2MSP.peer','Org3MSP.peer')"

```

The upgrade call adds a new block – block 6 – to the channel’s ledger and allows for the Org3 peers to execute transactions during the endorsement phase. Hop back to the Org3 CLI container and issue a query for the value of a.
This will take a bit of time because a chaincode image needs to be built for the targeted peer, and the container needs to start:
```
peer chaincode query -C $CHANNEL_NAME -n mycc -c '{"Args":["query","a"]}'
```
We should see a response of Query Result: 90.

Now issue an invocation to move 10 from a to b:
```
peer chaincode invoke -o orderer.example.com:7050  --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C $CHANNEL_NAME -n mycc -c '{"Args":["invoke","a","b","10"]}'
```

Query one final time:
```
peer chaincode query -C $CHANNEL_NAME -n mycc -c '{"Args":["query","a"]}'
```

We should see a response of Query Result: 80, accurately reflecting the update of this chaincode’s world state.



