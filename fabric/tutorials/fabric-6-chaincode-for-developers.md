# Chaincode for Developers
Normally chaincodes are started and maintained by peer. However in “dev mode”, chaincode is built and started by the user. This mode is useful during chaincode development phase for rapid code/build/run/debug cycle turnaround.

We start “dev mode” by leveraging pre-generated orderer and channel artifacts for a sample dev network. As such, the user can immediately jump into the process of compiling chaincode and driving calls.

## Terminal 1 - Start the network
```
cd chaincode-docker-devmode

docker-compose -f docker-compose-simple.yaml up
```

## Terminal 2- Build & start the chaincode
```
docker exec -it chaincode bash
```
**compile your chaincode:**
```
cd sacc
go build
```

Run the chaincode:
```
CORE_PEER_ADDRESS=peer:7052
CORE_CHAINCODE_ID_NAME=mycc:0 ./sacc
```
The chaincode is started with peer and chaincode logs indicating successful registration with the peer. Note that at this stage the chaincode is not associated with any channel. This is done in subsequent steps using the instantiate command.

## Terminal 3 - Use the chaincode
Even though you are in --peer-chaincodedev mode, you still have to install the chaincode so the life-cycle system chaincode can go through its checks normally. This requirement may be removed in future when in --peer-chaincodedev mode.

We’ll leverage the CLI container to drive these calls.

```
docker exec -it cli bash
```

```
peer chaincode install -p chaincodedev/chaincode/sacc -n mycc -v 0
peer chaincode instantiate -n mycc -v 0 -c '{"Args":["a","10"]}' -C myc
```

Now issue an invoke to change the value of “a” to “20”.
```
peer chaincode invoke -n mycc -c '{"Args":["set", "a", "20"]}' -C myc
```

## Chaincode access control
Chaincode can utilize the client (submitter) certificate for access control decisions by calling the GetCreator() function. Additionally the Go shim provides extension APIs that extract client identity from the submitter’s certificate that can be used for access control decisions, whether that is based on client identity itself, or the org identity, or on a client identity attribute.


See the [client identity (CID) library documentation](https://github.com/hyperledger/fabric/blob/master/core/chaincode/shim/ext/cid/README.md) for more details.

## Chaincode encryption
In certain scenarios, it may be useful to encrypt values associated with a key in their entirety or simply in part. For example, if a person’s social security number or address was being written to the ledger, then you likely would not want this data to appear in plaintext. Chaincode encryption is achieved by leveraging the  [entities extension](https://github.com/hyperledger/fabric/tree/master/core/chaincode/shim/ext/entities)  which is a BCCSP wrapper with commodity factories and functions to perform cryptographic operations such as encryption and elliptic curve digital signatures. For example, to encrypt, the invoker of a chaincode passes in a cryptographic key via the transient field. The same key may then be used for subsequent query operations, allowing for proper decryption of the encrypted state values.

For more information and samples, see the  [Encc Example](https://github.com/hyperledger/fabric/tree/master/examples/chaincode/go/enccc_example)  within the  `fabric/examples`  directory.



