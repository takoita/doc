# Writing Your First Application

## Déploiement du réseau blockchain
### Arrêter le réseau s'il est en cours d'exécution

```
cd first-network
./byfn.sh down
```

Il peut-être nécessaire par fois de faire plus radical:

```
docker rm -f $(docker ps -aq)
docker rmi -f $(docker images | grep fabcar | awk '{print $3}')
```

### Lancer le réseau

```
cd fabcar 
./startFabric.sh javascript
```

### Installer l'application javascript
NPM install ne marche pas dans vagrant sous windows à cause du support des liens symboliques dans les dossiers partagés par VirtualBox. Le workaround est soit de ne pas créer le projet Node.js dans le
dossier partagé, soit de lancer la commande npm install sous windows.
```
cd fabcar/javascript
npm install
```

## Enrôlement du user admin auprès du CA ca.example.com
```
node enrollAdmin.js
```

## Enregistrement et enrôlement du user user1
```
node registerUser.js
```

## Requête du ledger
```
node query.js
```

## Mise à jour du ledger
```
node invoke.js
```