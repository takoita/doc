# Key words

## Chaincodes
 * **BCCSP**: BCCSP is the blockchain cryptographic service provider that offers the implementation of cryptographic standards and algorithms.
 * **CDS**: ChaincodeDeploymentSpec
 * **[LSCC](https://github.com/hyperledger/fabric/tree/master/core/scc/lscc)**:  Lifecycle system chaincode handles lifecycle requests described above.
 * **[CSCC](https://github.com/hyperledger/fabric/tree/master/core/scc/cscc)**:  Configuration system chaincode handles channel configuration on the peer side.
 * **[QSCC](https://github.com/hyperledger/fabric/tree/master/core/scc/qscc)**:  Query system chaincode provides ledger query APIs such as getting blocks and transactions.