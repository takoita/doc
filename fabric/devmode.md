# Using dev mode (fabric 1.4)
 . https://github.com/hyperledger/fabric-samples/tree/release-1.4/chaincode-docker-devmode
 

The commands are executed from chaincode-docker-devmode

## Terminal 1 - Start the network
```
docker-compose -f docker-compose-simple.yaml up -d
```

## Terminal 2 - Build & start the chaincode

```
 $(npm bin)/fabric-chaincode-node start --peer.address localhost:7052 --chaincode-id-name MyAssetContract
```
