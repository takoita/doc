# Updating a Channel Configuration

## What is a Channel Configuration?
Channel configurations contain all of the information relevant to the administration of a channel. Most importantly, the channel configuration specifies which organizations are members of channel, but it also includes other channel-wide configuration information such as **channel access policies** and **block batch sizes**.

This configuration is stored on the ledger in a block, and is therefore known as a **configuration (config) block**. **Configuration blocks contain a single configuration**. The first of these blocks is known as the **“genesis block”** and contains the initial configuration required to bootstrap a channel. 

Because configurations are stored in blocks, updating a config happens through a process called a **“configuration transaction”** (even though the process is a little different from a normal transaction).  Updating a config is a process of:
 1. pulling the config
 2. translating into a format that humans can read
 3. modifying it and then submitting it for approval.
 
 ## Editing a Config
 A config might look intimidating in this form, but once you study it you’ll see that it has a logical structure :cold_sweat:
 
 Beyond the definitions of the policies – defining who can do certain things at the channel level, and who has the permission to change who can change the config – channels also have other kinds of features that can be modified using a config update. [Adding an Org to a Channel](https://hyperledger-fabric.readthedocs.io/en/latest/channel_update_tutorial.html) takes you through one of the most important – adding an org to a channel. Some other things that are possible to change with a config update include:
 
 - **Batch Size.** These parameters dictate the number and size of transactions in a block. No block will appear larger than `absolute_max_bytes` large or with more than `max_message_count`transactions inside the block. If it is possible to construct a block under `preferred_max_bytes`, then a block will be cut prematurely, and transactions larger than this size will appear in their own block.
 ```
 {
  "absolute_max_bytes": 102760448,
  "max_message_count": 10,
  "preferred_max_bytes": 524288
}
  ```
 
 - **Batch Timeout.** The amount of time to wait after the first transaction arrives for additional transactions before cutting a block. Decreasing this value will improve latency, but decreasing it too much may decrease throughput by not allowing the block to fill to its maximum capacity.
 ```
 { "timeout": "2s" }
 ```
 
 - **Channel Restrictions.** The total number of channels the orderer is willing to allocate may be specified as max_count. This is primarily useful in pre-production environments with weak consortium `ChannelCreation` policies.
   ```
    {
      "max_count":1000
    }
  ```
 
 - **Channel Creation Policy.** The total number of channels the orderer is willing to allocate may be specified as max_count. This is primarily useful in pre-production environments with weak consortium `ChannelCreation` policies.
 ```
    {
      "type": 3,
      "value": {
      "rule": "ANY",
      "sub_policy": "Admins"
    }
    }
   ```

- **Kafka brokers.** When `ConsensusType` is set to `kafka`, the `brokers` list enumerates some subset (or preferably all) of the Kafka brokers for the orderer to initially connect to at startup. _Note that it is not possible to change your consensus type after it has been established (during the bootstrapping of the genesis block)_.
   ```
    {
    "brokers": [
    "kafka0:9092",
    "kafka1:9092",
    "kafka2:9092",
    "kafka3:9092"
    ]
   }
   ```

- **Hashing Structure.**  The block data is an array of byte arrays. The hash of the block data is computed as a Merkle tree. This value specifies the width of that Merkle tree. For the time being, this value is fixed to  `4294967295`  which corresponds to a simple flat hash of the concatenation of the block data bytes.
  ```
    { "width": 4294967295 }
   ```

- **Hashing Algorithm.** The algorithm used for computing the hash values encoded into the blocks of the blockchain. In particular, this affects the data hash, and the previous block hash fields of the block. Note, this field currently only has one valid value (`SHA256`) and should not be changed.
    ```
      { "name": "SHA256" }
    ```

- **Block Validation.** This policy specifies the signature requirements for a block to be considered valid. By default, it requires a signature from some member of the ordering org.
    ```
      {
        "type": 3,
        "value": {
        "rule": "ANY",
        "sub_policy": "Writers"
           }
         }
      ```
- **Orderer Address.** A list of addresses where clients may invoke the orderer `Broadcast` and `Deliver` functions. The peer randomly chooses among these addresses and fails over between them for retrieving blocks.

- **Orderer Address.** A list of addresses where clients may invoke the orderer `Broadcast` and `Deliver` functions. The peer randomly chooses among these addresses and fails over between them for retrieving blocks.
    ```
     {
           "addresses": [
           "orderer.example.com:7050"
     ]
      }
   ```


Just as we add an Org by adding their artifacts and MSP information, you can remove them by reversing the process.

**Note** that once the consensus type has been defined and the network has been bootstrapped, it is not possible to change it through a configuration update.

### Let’s say you want to edit the block batch size for the channel (because this is a single numeric field, it’s one of the easiest changes to make)

Open a terminal on cli container:

```
docker exec -it cli bash 
```

Export the ORDERER_CA and CHANNEL_NAME variables:

```
export ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
export CHANNEL_NAME=mychannel
```

Fetch and convert the last config block in json:
```
peer channel fetch config config_block.pb -o orderer.example.com:7050 -c $CHANNEL_NAME --tls --cafile $ORDERER_CA

configtxlator proto_decode --input config_block.pb --type common.Block | jq .data.data[0].payload.data.config > config.json
``` 

Find and store as an environment variable from the JSON config file  the  JSON path of the block batch size attribute:

```
 export MAXBATCHSIZEPATH=".channel_group.groups.Orderer.values.BatchSize.value.max_message_count"
```

Next, display the value of that property:

```
jq "$MAXBATCHSIZEPATH" config.json
```

Which should return a value of 10 (in our sample network at least).

Now, let’s set the new batch size and display the new value:

```
 jq “$MAXBATCHSIZEPATH = 20” config.json > modified_config.json
 jq “$MAXBATCHSIZEPATH” modified_config.json
```

Next, encode modified_config.json to modified_config.pb:
```
configtxlator proto_encode --input modified_config.json --type common.Config --output modified_config.pb
```

And encode config.json back into a protobuf called config.pb:

```
configtxlator proto_encode --input config.json --type common.Config --output config.pb
```


Now use configtxlator to calculate the delta between these two config protobufs. This command will output a new protobuf binary named batch_size_update.pb:

```
configtxlator compute_update --channel_id $CHANNEL_NAME --original config.pb --updated modified_config.pb --output batch_size_update.pb
```

Before submitting the channel update, we need to perform a few final steps. First, let’s decode this object into editable JSON format and call it batch_size_update.json:

```
configtxlator proto_decode --input batch_size_update.pb --type common.ConfigUpdate | jq . > batch_size_update.json
```

Now, we have a decoded update file – batch_size_update.json – that we need to wrap in an envelope message. This step will give us back the header field that we stripped away earlier. We’ll name this file batch_size_update_in_envelope.json:

```
echo '{"payload":{"header":{"channel_header":{"channel_id":"mychannel", "type":2}},"data":{"config_update":'$(cat batch_size_update.json)'}}}' | jq . > batch_size_update_in_envelope.json
```

Using our properly formed JSON – batch_size_update_in_envelope.json – we will leverage the configtxlator tool one last time and convert it into the fully fledged protobuf format that Fabric requires. We’ll name our final update object batch_size_update_in_envelope.pb:

```
configtxlator proto_encode --input batch_size_update_in_envelope.json --type common.Envelope --output batch_size_update_in_envelope.pb
```

### Get the Necessary Signatures
Once you’ve successfully generated the protobuf file, it’s time to get it signed. To do this, you need to know the relevant policy for whatever it is you’re trying to change.

By default, editing the configuration of:

 * **A particular org**  (for example, changing anchor peers) requires only the admin signature of that org.
- **The application**  (like who the member orgs are) requires a majority of the application organizations’ admins to sign.
- **The orderer**  requires a majority of the ordering organizations’ admins (of which there are by default only 1).
- **The top level  `channel`  group**  requires both the agreement of a majority of application organization admins and orderer organization admins.
 

If you have made changes to the default policies in the channel, you’ll need to compute your signature requirements accordingly.

 > Note: you may be able to script the signature collection, dependent on your application. In general, you may always collect more
 > signatures than are required._

The actual process of getting these signatures will depend on how you’ve set up your system, but there are two main implementations. Currently, the Fabric command line defaults to a “pass it along” system. That is, the Admin of the Org proposing a config update sends the update to someone else (another Admin, typically) who needs to sign it. This Admin signs it (or doesn’t) and passes it along to the next Admin, and so on, until there are enough signatures for the config to be submitted.

This has the virtue of simplicity – when there are enough signatures, the last Admin can simply submit the config transaction (in Fabric, the  `peer  channel  update`  command includes a signature by default). However, this process will only be practical in smaller channels, since the “pass it along” method can be time consuming.

The other option is to submit the update to every Admin on a channel and wait for enough signatures to come back. These signatures can then be stitched together and submitted. This makes life a bit more difficult for the Admin who created the config update (forcing them to deal with a file per signer) but is the recommended workflow for users which are developing Fabric management applications.

Once the config has been added to the ledger, it will be a best practice to pull it and convert it to JSON to check to make sure everything was added correctly. This will also serve as a useful copy of the latest config.


Sign and Submit the Config Update. First, let’s sign this update proto as the Org1 Admin. Remember that the CLI container is bootstrapped with the Org1 MSP material, so we simply need to issue the peer channel signconfigtx command:

```
peer channel signconfigtx -f batch_size_update_in_envelope.pb
```

Export the Org2 environment variables:

```
export CORE_PEER_LOCALMSPID="Org2MSP"

export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt

export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp

export CORE_PEER_ADDRESS=peer0.org2.example.com:7051
```

Lastly, we will issue the peer channel update command. The Org2 Admin signature will be attached to this call so there is no need to manually sign the protobuf a second time:

```
peer channel update -f batch_size_update_in_envelope.pb -c $CHANNEL_NAME -o orderer.example.com:7050 --tls --cafile $ORDERER_CA
```



