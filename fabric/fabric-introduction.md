# My notes from the official doc of Hyperledger Fabric: https://hyperledger-fabric.readthedocs.io/en/latest/index.html

## Introduction
###Blockchain Definition
Une blockchain est un registre dont les copies sont réparties sur plusieurs pairs d'un réseau qui communiquent à travers un protocole de consensus
pour maintenir le même état du registre. L'appelation *blockchain* vient du fait que les données du regsitre sont stockées sous forme de blocs, 
chaque bloc de données referençant le précédent avec le hash de celui-ci. Ce chainage des blocs à travers leurs hashs tend à conférer à la blockhain
une propriété d'immuabilité, car toute tentative de modification du contenu d'un bloc modifiera son hash, ce qui sera facilement détecté par les autres
pairs du réseau car l'anciennece valeur du hash reste stockée dans le block suivant.

##Blockchain en entreprise
**Bitcoin** et **Ethereum**, deux des blockchains les plus connues et les plus utilisées sont ouvertes à l'utilisation de n'importe quel individu, elles rentrent dans la catégorie ce ce qu'on appelle
**blockchain sans permission** ou **blockhain publique**. Tandis que les blockains publiques ont l'avantage de permettre à tout le monde sans exclusion(moyennant la possesion d'un appareil connecté à Internet)
de bénéficier de leurs services, les entreprises qui sont désormains très intéressées par l'usage de la blockchain ont des contraintes qui les oblige à controler les accès à leurs réseaux et
à connaitre les auteurs des actions. Ces containtres peuvent être énumérées plus explicitement comme suit:

* Les participants doivent être identifiés
* Les accès aux réseaux doivent êtres sousmis à des contrôles
* Un besoin de performance acceptable
* Donc des temps d'exécution des  transactions acceptables
* Confidentialité des transactions et des données impliquées.

**Hyperledger Fabric** est une technologie de blockchain concue spécifiquement pour répondre à ces contraintes afin de permettre la construction de blockchains d'entreprises.

##Hyperledger Fabric
Hyperledger Fabric est donc une technologie de blockchain *open source* de type *permissioned* destinée à la construction de blockains en entreprise.

Hyperledger Fabric à l'origine développée par IBM, a été confié à la fonction Linux où il évolue désormais dans le cadre du projet englobant Hyperledger. Hyperledger fournit un écosystème de solutions
pour l'utilisation de technologies blockchains. L''évolution dans ce contexte permet au projet de voir l'implication de nombreux acteurs provenant de dizaines organisations diverses.

* **Modularité et configurabilité**:  Fabric a une architecture modulaire et configurable, ce qui facilite son évolution et le rend versalie et opmisable pour une large variété d'usages dans les industries: banque, finance, assurance, santé, ressources humaines, chaine logistique et même le streaming mulimdeia.
* Ecriture de smart contract dans les **langages de programmation d'usage général**: Fabric est la première plateforme de ledger distribué ayant permis l(écriture de smart contracts dans les langages de programmtion courants comme Java, Go et Node.js.
* **Permissioned**: Fabric permet de contrôler les accès des participants qui doivent avoir des identités connues dans le système.
* **Protocole de consensus pluggable**:  Fabric supporte l'activation de différents ptotocoles de consenus, le choix est laissé aux utlisateurs en fonction du contexte. Dans un contexte où les participants font tous partie de la même entreprise, un protocole de consensus simple comme le Crash Fault-Tolerant peut-être adopté pour éviter des pénalités de performance injustifiées. Et dans un contexte où les acteurs sont plus décentralisés, il conviendra alors d'utiliser le protocle de consensus approprié comme le Byzantine Fault Tolerant(BFT).
* **Protocole de consenus sans minage de monnai**: L'absence de facteur d'incitation à l'établissement de consensus par minage de crypto-monnaie simplifie le fonctionnement de l'établisemnt du consensus, et donc de toute la plateforme.

L'ensembe de ces points font de Fabric l'une des meilleurs plateformes disponibles à l'heure actuelle  en termes de latence de l'exécution des transactions, ils permettent la confidentialité  des transactions et des smart contracts qui les implémentent.

## Concepts Clés
### Qu'est-ce que la blockchain?
#### A Distributed Ledger
Au coeur de la blockchain se trouve le registre distribué qui enregistre toutes les transactions produites sur le réseau de la blockchain. Ce registre distribué ne peut être modifié qu'en y ajoutant 
des informations, pas en en supprimmant ou en en modifiant, pour cela il est dit immuable. Cette immuabilité est guarantie par l'utilisation de techniques de cryptograhie. Parce qu'il fournit la certitude d'être immuable,
la blockchain simplifie la fiabilisation des informations, raison pour laquelle elle souvent appelée **système de preuve**.

#### Smart Contracts
Le ledger est modifié ou consulté par les participants à travers des transactions que constituent les smarts contracts.

#### Consensus
Le consensus est le protocole qui permet de maintenir toutes les replications du ledger dans le même étant lors de l'exécution des transactions déclenchées et approuvées par les participants du réseau.

### En quoi la blockchain est-elle utile?
La manière d'enregistrer les transactions dans les réseaux d'affaires est la même que depuis les époques où nos ancètres le faisait en utilisant des tablettes à écrire ou plus récemment du papier. Les transactions
sont enregistrées de la même façon de nos jours, même si elles le sont à présent sur des supports plus évolués, comme les disques durs. Cette manière consiste au fait que chaque participant possède sa propre
copie privée de ledger, qu'il met à jour durant ses transactions. De ce fait il n'existe pas de vue unifiée dans le réseau sur les bien qui sont échangés et par qui, quand et où ils sont échangés. Cette absence de vue unifiée
peut-être source de ralentissemement considérable da,s me mécanisme de compensation par exemple. Un autre souci de ce système est que les informations dans le ledger de chaque participant étant unique, ils devients tous des 
points individuels de défaillance.


La revolution promise par la blockchain est qu'elle incite les acteurs d'un réseau à partager le même régistre afin d'avoir la même vue à tout moment sur les transactions effectuées par chaque participant. La
blockchain apportant la guarantie de  fiabilité des informations présentes, le temps, le coût et le rique associées aux prises de décisions sont réduits.

## Fonctionnalités de Hyperledger Fabric
1. ### Gestion  d'identité
     Pour permettre le contrôle des accès au réseau, Fabric fournit le MemberShip Service Provider(MSP) qui gère les identités et effectue l'aithentification des utilisateurs. Les ACL peuvent aussi être utilisés pour au niveau couches de permissions supplémentaires pour l'accès à certaines opérations.


2. ### Privacy et confidentialité
Fabric permet à des acteurs en compétition ou exigeant la confidentialité des transactions  de coexister sur le même réseau. Les cannaux(channels) sont un mécanisme de voie de communication fourni par 
Fabric pour garantir la conidentialité des transactions de certains membres du réseau. Les données, les transactions, les membres et les information du cannal sont inaccessibles aux membres du réseau
qui n'ont pas l'autorisation d'accès au canal.


3. ### Optimisation des traitements
Il existe différents types de noeuds fournissant différents services dans Fabric, cela permet de paralléiser les tâches qui n'ont pas besoin d'être sérailisées, comme l'exécution des transaction 
et leur classement ou leur enregistrement. Ce parallélisme optimise évidemment l'éfficacité du processus de traitement sur l'ensemble du réseau.


4. ### Fonctionnalité de chaincode
Les logiques métiers pour accéder ou modifier le ledger sobt implémentées dans les chaincodes. Il existe des **System chaincode** qui fournissent des services techniques pour la gestion des chaincoddes métiers:
  * Lifecycle and configuration system chaincode(LCCC et CSCC): 
  * Endorsement and Aalidation System Chaincode(EVSCC)
  
  
5. ### Conception modulaire
L'arhcitecture mudlaire de Fabric permet aux développeurs de choisir les composants qui leur paraissent appropriés dans leur contexte du'ilisation.
Des algorithmes spéicifiques peuvent être utilisés pour l'identité, pour le consensus,, etc. 

## Modèle de Hyperledger Fabric
## Assets
## Chaincode
## Ledger
## Confidentialité
## Sécrurité et Membership Services
## Consenus

## Blockchain network
### Qu'est ce qu'un réseau blockchain?
Un réseau blockchain est une infrastructure technique qui fournit des services de ledger et smart contract aux applications. Les utilisateurs de la blockchain sont ceux accédant à ces application et ceux 
qui administrent le réseau de blockchain.

Dans de nombreux cas, les organisations se constituent en consortium pour former un réseau blockchain et les permissions du réseau sont detrminées par un ensemble de règles acceptées par le consortium lorsque le réseau est originellement configuré.
En outre, les politique du réseau peuvent changer au cours du temps en fonction des accords entre les organisations du consortium, comme nous allons le découvrir.

## Identité
###Qu'est-ce qu'une identité?












