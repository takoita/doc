# [Dloping Hyperledger Fabric Applications: PaperNet network ](https://hyperledger-fabric.readthedocs.io/en/latest/developapps/developing_applications.html)

## Analyse
### Cycle de vie d'un Commercial Paper
### Transactions

## Conception des traitements et des données
### Cycle de vie
### Etat du ledger
### Clés des états
### Etats multiples

## Traitement des smart contracts
### Smart contract
### Classe du contrat
### Défintion de la transaction
### Logique de la transaction
### Représentation d'un objet
### Accès au ledger

## Applications clients du réseau de blockchain
### Flow d'invocation d'un smart contract
### gestion de l'identité par un wallet
### Connection au réseau de blokchain par un gateway
### Accès à un réseau blockchain particulier
### Construction d'une requête de transaction
### Traitement d'une réponse d'une transaction





