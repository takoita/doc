# Préparation d'un environnement Fabric
##Prerequis
### cURL
```
sudo apt-get install cURL --yes
```
#### Docker et Docker Compose(version >=1.14.0)
```
#docker
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo groupadd docker
sudo usermod -aG docker vagrant
sudo systemctl enable docker	

#docker compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose    
```

###Go(version >=1.11.x)
```
curl -O https://storage.googleapis.com/golang/go1.11.2.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go1.11.2.linux-amd64.tar.gz	

# Ajouter les binaires de GO au PATH
echo 'export PATH=${PATH}:/usr/local/go/bin' >> /home/vagrant/.bashrc
```

### Node.js Runtime and NPM(Node.js version 8.x)
L'environnement Node.js est requis pour l'utilisation du client Node SDK de Fabric. Node.js peut être installé directeemnt ou à partir de l'utilitaire
[nvm](https://github.com/creationix/nvm)(Node Version Manager).
```
# le script clone le repo nvm dans ~/.nvm et ajoute les chemins des binaires dans ~/.bashrc ou ~/.profile, etc.
# le repertoire d'installation de nvm peut être précisé en définissant la variable XDG_CONFIG_HOME
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash

# Installer à présent Node.js

nvm install 8.9.1

#g++: utilisé dans les applications Node.js
sudo apt-get install g++ --yes	
```

### Python 2.7
Le client Node SDK de Fabric requiert Python 2.7.
```
sudo apt-get install python --yes
```
## Installation des exemples, binaires(commandes) et images docker de Fabric
En attendant de fournir de vrais programmes d'installation des binaires de Fabric, les équipes de Fabric fournissent 
un script qui permmet d'installer en une seule fois: des examples de projet Fabric, les binaires(commandes) de Fabric et les images dockers des différents composants de Fabric.

Il suffit de se positioner dans le repertoire où l'on veut installer les projets et les binaires et de lancer le script en précisant a version désirée de Fabric:
```
FABRIC_HOME=~/app/hyperledger/fabric-home
mkdir -p $FABRIC_HOME && cd $FABRIC_HOME
curl -sSL http://bit.ly/2ysbOFE | bash
# des versions par défaut sont  fixées dans le script, normalement la dernière disponible de Fabric
# pour lancer le script en précisant la version de Fabric à installer
curl -sSL http://bit.ly/2ysbOFE | bash -s 1.4.0-rc2
```
Ajouter les binaires de Fabric au PATH:
```
echo 'export PATH=${PATH}:$FABRIC_HOME/fabric-samples/bin' >> /home/vagrant/.bashrc
```
## VM Vagrant avec l'environnement Fabric
Toutes ces commandes d'installation sont rassemblées dans le Vagrantfile qui permet donc de lancer une VM Linux avec un environnement Fabric prêt à être utilisé: [Vagranfile](https://bitbucket.org/takoita/vagrant/raw/d8eedc1daa896ab9572ad1f512bbc871944109dd/Vagrantfile).


