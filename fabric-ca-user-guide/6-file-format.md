# File Formats
## Fabric CA server’s configuration file format
A default configuration file is created in the server’s home directory (see [Fabric CA Server](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#server)section for more info). The following link shows a sample [Server configuration file](https://hyperledger-fabric-ca.readthedocs.io/en/latest/serverconfig.html).

## Fabric CA client’s configuration file format
A default configuration file is created in the client’s home directory (see [Fabric CA Client](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#client) section for more info). The following link shows a sample [Client configuration file](https://hyperledger-fabric-ca.readthedocs.io/en/latest/clientconfig.html).