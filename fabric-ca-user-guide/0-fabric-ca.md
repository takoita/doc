The Hyperledger Fabric CA is a Certificate Authority (CA) for Hyperledger Fabric.

It provides features such as:

> -   registration of identities, or connects to LDAP as the user registry
> -   issuance of Enrollment Certificates (ECerts)
> -   certificate renewal and revocation

Hyperledger Fabric CA consists of both a server and a client component as described later in this document.

For developers interested in contributing to Hyperledger Fabric CA, see the  [Fabric CA repository](https://github.com/hyperledger/fabric-ca)  for more information.

## Table of Contents
1.  [Overview](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#overview)
2.  [Getting Started](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#getting-started)
    1.  [Prerequisites](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#prerequisites)
    2.  [Install](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#install)
    3.  [Explore the Fabric CA CLI](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#explore-the-fabric-ca-cli)
3.  [Configuration Settings](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#configuration-settings)
    1.  [A word on file paths](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#a-word-on-file-paths)
4.  [Fabric CA Server](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#fabric-ca-server)
    1.  [Initializing the server](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#initializing-the-server)
    2.  [Starting the server](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#starting-the-server)
    3.  [Configuring the database](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#configuring-the-database)
    4.  [Configuring LDAP](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#configuring-ldap)
    5.  [Setting up a cluster](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#setting-up-a-cluster)
    6.  [Setting up multiple CAs](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#setting-up-multiple-cas)
    7.  [Enrolling an intermediate CA](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#enrolling-an-intermediate-ca)
    8.  [Upgrading the server](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#upgrading-the-server)
5.  [Fabric CA Client](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#fabric-ca-client)
    1.  [Enrolling the bootstrap identity](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#enrolling-the-bootstrap-identity)
    2.  [Registering a new identity](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#registering-a-new-identity)
    3.  [Enrolling a peer identity](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#enrolling-a-peer-identity)
    4.  [Getting Identity Mixer credential for a user](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#getting-identity-mixer-credential-for-a-user)
    5.  [`Getting Idemix CRI`_](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#id1)
    6.  [Reenrolling an identity](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#reenrolling-an-identity)
    7.  [Revoking a certificate or identity](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#revoking-a-certificate-or-identity)
    8.  [Generating a CRL (Certificate Revocation List)](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#generating-a-crl-certificate-revocation-list)
    9.  [Attribute-Based Access Control](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#attribute-based-access-control)
    10.  [Dynamic Server Configuration Update](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#dynamic-server-configuration-update)
    11.  [Enabling TLS](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#enabling-tls)
    12.  [Contact specific CA instance](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#contact-specific-ca-instance)
6.  [HSM](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#hsm)
    1.  [Configuring Fabric CA server to use softhsm2](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#configuring-fabric-ca-server-to-use-softhsm2)
7.  [File Formats](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#file-formats)
    1.  [Fabric CA server’s configuration file format](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#fabric-ca-server-s-configuration-file-format)
    2.  [Fabric CA client’s configuration file format](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#fabric-ca-client-s-configuration-file-format)
8.  [Troubleshooting](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#troubleshooting)