# Fabric CA Client
This section describes how to use the fabric-ca-client command.

The Fabric CA client’s home directory is determined as follows:

-   if the –home command line option is set, use its value
-   otherwise, if the  `FABRIC_CA_CLIENT_HOME`  environment variable is set, use its value
-   otherwise, if the  `FABRIC_CA_HOME`  environment variable is set, use its value
-   otherwise, if the  `CA_CFG_PATH`  environment variable is set, use its value
-   otherwise, use  `$HOME/.fabric-ca-client`

The instructions below assume that the client configuration file exists in the client’s home directory.

## Enrolling the bootstrap identity
First, if needed, customize the CSR (Certificate Signing Request) section in the client configuration file. Note that `csr.cn` field must be set to the ID of the bootstrap identity. Default CSR values are shown below:

    csr:
      cn: <<enrollment ID>>
      key:
        algo: ecdsa
        size: 256
      names:
        - C: US
          ST: North Carolina
          L:
          O: Hyperledger Fabric
          OU: Fabric CA
      hosts:
       - <<hostname of the fabric-ca-client>>
      ca:
        pathlen:
        pathlenzero:
        expiry:


Then run `fabric-ca-client  enroll` command to enroll the identity. For example, following command enrolls an identity whose ID is **admin** and password is **adminpw** by calling Fabric CA server that is running locally at 7054 port.

    export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca/clients/admin
    fabric-ca-client enroll -u http://admin:adminpw@localhost:7054

The enroll command stores an enrollment certificate (ECert), corresponding private key and CA certificate chain PEM files in the subdirectories of the Fabric CA client’s `msp` directory. You will see messages indicating where the PEM files are stored.

## Registering a new identity
The identity performing the register request must be currently enrolled, and must also have the proper authority to register the type of the identity that is being registered.


In particular, three authorization checks are made by the Fabric CA server during registration as follows:

1.  The registrar (i.e. the invoker) must have the “hf.Registrar.Roles” attribute with a comma-separated list of values where one of the values equals the type of identity being registered; for example, if the registrar has the “hf.Registrar.Roles” attribute with a value of “peer,app,user”, the registrar can register identities of type peer, app, and user, but not orderer.
2.  The affiliation of the registrar must be equal to or a prefix of the affiliation of the identity being registered. For example, an registrar with an affiliation of “a.b” may register an identity with an affiliation of “a.b.c” but may not register an identity with an affiliation of “a.c”. If root affiliation is required for an identity, then the affiliation request should be a dot (”.”) and the registrar must also have root affiliation. If no affiliation is specified in the registration request, the identity being registered will be given the affiliation of the registrar.
3.  The registrar can register a user with attributes if all of the following conditions are satisfied:
    -   Registrar can register Fabric CA reserved attributes that have the prefix ‘hf.’ only if the registrar possesses the attribute and it is part of the value of the hf.Registrar.Attributes’ attribute. Furthermore, if the attribute is of type list then the value of attribute being registered must be equal to or a subset of the value that the registrar has. If the attribute is of type boolean, the registrar can register the attribute only if the registrar’s value for the attribute is ‘true’.
    -   Registering custom attributes (i.e. any attribute whose name does not begin with ‘hf.’) requires that the registrar has the ‘hf.Registar.Attributes’ attribute with the value of the attribute or pattern being registered. The only supported pattern is a string with a “*” at the end. For example, “a.b.*” is a pattern which matches all attribute names beginning with “a.b.”. For example, if the registrar has hf.Registrar.Attributes=orgAdmin, then the only attribute which the registrar can add or remove from an identity is the ‘orgAdmin’ attribute.
    -   If the requested attribute name is ‘hf.Registrar.Attributes’, an additional check is performed to see if the requested values for this attribute are equal to or a subset of the registrar’s values for ‘hf.Registrar.Attributes’. For this to be true, each requested value must match a value in the registrar’s value for ‘hf.Registrar.Attributes’ attribute. For example, if the registrar’s value for ‘hf.Registrar.Attributes’ is ‘a.b.*, x.y.z’ and the requested attribute value is ‘a.b.c, x.y.z’, it is valid because ‘a.b.c’ matches ‘a.b.*’ and ‘x.y.z’ matches the registrar’s ‘x.y.z’ value.

Examples:

Valid Scenarios:

1.  If the registrar has the attribute ‘hf.Registrar.Attributes = a.b.*, x.y.z’ and is registering attribute ‘a.b.c’, it is valid ‘a.b.c’ matches ‘a.b.*’.
2.  If the registrar has the attribute ‘hf.Registrar.Attributes = a.b.*, x.y.z’ and is registering attribute ‘x.y.z’, it is valid because ‘x.y.z’ matches the registrar’s ‘x.y.z’ value.
3.  If the registrar has the attribute ‘hf.Registrar.Attributes = a.b.*, x.y.z’ and the requested attribute value is ‘a.b.c, x.y.z’, it is valid because ‘a.b.c’ matches ‘a.b.*’ and ‘x.y.z’ matches the registrar’s ‘x.y.z’ value.
4.  If the registrar has the attribute ‘hf.Registrar.Roles = peer,client’ and the requested attribute value is ‘peer’ or ‘peer,client’, it is valid because the requested value is equal to or a subset of the registrar’s value.

Invalid Scenarios:

1.  If the registrar has the attribute ‘hf.Registrar.Attributes = a.b.*, x.y.z’ and is registering attribute ‘hf.Registar.Attributes = a.b.c, x.y.*’, it is invalid because requested attribute ‘x.y.*’ is not a pattern owned by the registrar. The value ‘x.y.*’ is a superset of ‘x.y.z’.
2.  If the registrar has the attribute ‘hf.Registrar.Attributes = a.b.*, x.y.z’ and is registering attribute ‘hf.Registar.Attributes = a.b.c, x.y.z, attr1’, it is invalid because the registrar’s ‘hf.Registrar.Attributes’ attribute values do not contain ‘attr1’.
3.  If the registrar has the attribute ‘hf.Registrar.Attributes = a.b.*, x.y.z’ and is registering attribute ‘a.b’, it is invalid because the value ‘a.b’ is not contained in ‘a.b.*’.
4.  If the registrar has the attribute ‘hf.Registrar.Attributes = a.b.*, x.y.z’ and is registering attribute ‘x.y’, it is invalid because ‘x.y’ is not contained by ‘x.y.z’.
5.  If the registrar has the attribute ‘hf.Registrar.Roles = peer,client’ and the requested attribute value is ‘peer,client,orderer’, it is invalid because the registrar does not have the orderer role in its value of hf.Registrar.Roles attribute.
6.  If the registrar has the attribute ‘hf.Revoker = false’ and the requested attribute value is ‘true’, it is invalid because the hf.Revoker attribute is a boolean attribute and the registrar’s value for the attribute is not ‘true’.

The table below lists all the attributes that can be registered for an identity. The names of attributes are case sensitive.

**Name**|**Type**|**Description**
:-----:|:-----:|:-----:
hf.Registrar.Roles|List|List of roles that the registrar is allowed to manage
hf.Registrar.DelegateRoles|List|List of roles that the registrar is allowed to give to a registree for its ‘hf.Registrar.Roles’ attribute
hf.Registrar.Attributes|List|List of attributes that registrar is allowed to register
hf.GenCRL|Boolean|Identity is able to generate CRL if attribute value is true
hf.Revoker|Boolean|Identity is able to revoke a user and/or certificates if attribute value is true
hf.AffiliationMgr|Boolean|Identity is able to manage affiliations if attribute value is true
hf.IntermediateCA|Boolean|Identity is able to enroll as an intermediate CA if attribute value is true


Note: When registering an identity, you specify an array of attribute names and values. If the array specifies multiple array elements with the same name, only the last element is currently used. In other words, multi-valued attributes are not currently supported.

The following command uses the  **admin**  identity’s credentials to register a new user with an enrollment id of “admin2”, an affiliation of “org1.department1”, an attribute named “hf.Revoker” with a value of “true”, and an attribute named “admin” with a value of “true”. The ”:ecert” suffix means that by default the “admin” attribute and its value will be inserted into the user’s enrollment certificate, which can then be used to make access control decisions.

    export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca/clients/admin
    fabric-ca-client register --id.name admin2 --id.affiliation org1.department1 --id.attrs 'hf.Revoker=true,admin=true:ecert'


The password, also known as the enrollment secret, is printed. This password is required to enroll the identity. This allows an administrator to register an identity and give the enrollment ID and the secret to someone else to enroll the identity.

Multiple attributes can be specified as part of the –id.attrs flag, each attribute must be comma separated. For an attribute value that contains a comma, the attribute must be encapsulated in double quotes. See example below.

    fabric-ca-client register -d --id.name admin2 --id.affiliation org1.department1 --id.attrs '"hf.Registrar.Roles=peer,user",hf.Revoker=true'

or

    fabric-ca-client register -d --id.name admin2 --id.affiliation org1.department1 --id.attrs '"hf.Registrar.Roles=peer,user"' --id.attrs hf.Revoker=true


You may set default values for any of the fields used in the register command by editing the client’s configuration file. For example, suppose the configuration file contains the following:

    id:
      name:
      type: user
      affiliation: org1.department1
      maxenrollments: -1
      attributes:
        - name: hf.Revoker
          value: true
        - name: anotherAttrName
          value: anotherAttrValue


The following command would then register a new identity with an enrollment id of “admin3” which it takes from the command line, and the remainder is taken from the configuration file including the identity type: “user”, affiliation: “org1.department1”, and two attributes: “hf.Revoker” and “anotherAttrName”.

    export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca/clients/admin
    fabric-ca-client register --id.name admin3

To register an identity with multiple attributes requires specifying all attribute names and values in the configuration file as shown above.

Setting  maxenrollments  to 0 or leaving it out from the configuration will result in the identity being registered to use the CA’s max enrollment value. Furthermore, the max enrollment value for an identity being registered cannot exceed the CA’s max enrollment value. For example, if the CA’s max enrollment value is 5. Any new identity must have a value less than or equal to 5, and also can’t set it to -1 (infinite enrollments).

Next, let’s register a peer identity which will be used to enroll the peer in the following section. The following command registers the  **peer1**  identity. Note that we choose to specify our own password (or secret) rather than letting the server generate one for us.

    export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca/clients/admin
    fabric-ca-client register --id.name peer1 --id.type peer --id.affiliation org1.department1 --id.secret peer1pw

Note that affiliations are case sensitive except for the non-leaf affiliations that are specified in the server configuration file, which are always stored in lower case. For example, if the affiliations section of the server configuration file looks like this:

    affiliations:
      BU1:
        Department1:
          - Team1
      BU2:
        - Department2
        - Department3


BU1, Department1, BU2 are stored in lower case. This is because Fabric CA uses Viper to read configuration. Viper treats map keys as case insensitive and always returns lowercase value. To register an identity with Team1 affiliation, bu1.department1.Team1 would need to be specified to the –id.affiliation flag as shown below:

    export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca/clients/admin
    fabric-ca-client register --id.name client1 --id.type client --id.affiliation bu1.department1.Team1


## Enrolling a peer identity
Now that you have successfully registered a peer identity, you may now enroll the peer given the enrollment ID and secret (i.e. the _password_ from the previous section). This is similar to enrolling the bootstrap identity except that we also demonstrate how to use the “-M” option to populate the Hyperledger Fabric MSP (Membership Service Provider) directory structure.

The following command enrolls peer1. Be sure to replace the value of the “-M” option with the path to your peer’s MSP directory which is the ‘mspConfigPath’ setting in the peer’s core.yaml file. You may also set the FABRIC_CA_CLIENT_HOME to the home directory of your peer.

    export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca/clients/peer1
    fabric-ca-client enroll -u http://peer1:peer1pw@localhost:7054 -M $FABRIC_CA_CLIENT_HOME/msp

Enrolling an orderer is the same, except the path to the MSP directory is the ‘LocalMSPDir’ setting in your orderer’s orderer.yaml file.

All enrollment certificates issued by the fabric-ca-server have organizational units (or “OUs” for short) as follows:

1.  The root of the OU hierarchy equals the identity type
2.  An OU is added for each component of the identity’s affiliation

For example, if an identity is of type  peer  and its affiliation is  department1.team1, the identity’s OU hierarchy (from leaf to root) is  OU=team1, OU=department1, OU=peer.

## Getting a CA certificate chain from another Fabric CA server
In general, the cacerts directory of the MSP directory must contain the certificate authority chains of other certificate authorities, representing all of the roots of trust for the peer.

The  `fabric-ca-client  getcainfo`  command is used to retrieve these certificate chains from other Fabric CA server instances.

For example, the following will start a second Fabric CA server on localhost listening on port 7055 with a name of “CA2”. This represents a completely separate root of trust and would be managed by a different member on the blockchain.

    export FABRIC_CA_SERVER_HOME=$HOME/ca2
    fabric-ca-server start -b admin:ca2pw -p 7055 -n CA2


The following command will install CA2’s certificate chain into peer1’s MSP directory.

    export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca/clients/peer1
    fabric-ca-client getcainfo -u http://localhost:7055 -M $FABRIC_CA_CLIENT_HOME/msp

By default, the Fabric CA server returns the CA chain in child-first order. This means that each CA certificate in the chain is followed by its issuer’s CA certificate. If you need the Fabric CA server to return the CA chain in the opposite order, then set the environment variable `CA_CHAIN_PARENT_FIRST` to `true` and restart the Fabric CA server. The Fabric CA client will handle either order appropriately.


### Getting Identity Mixer credential for a user
Identity Mixer (Idemix) is a cryptographic protocol suite for privacy-preserving authentication and transfer of certified attributes. Idemix allows users to authenticate with verifiers without the involvement of the issuer (CA) and selectively disclose only those attributes that are required by the verifier and can do so without being linkable across their transactions.

Fabric CA server can issue Idemix credentials in addition to X509 certificates. An Idemix credential can be requested by sending the request to the `/api/v1/idemix/credential` API endpoint. For more information on this and other Fabric CA server API endpoints, please refer to [swagger-fabric-ca.json](https://github.com/hyperledger/fabric-ca/blob/master/swagger/swagger-fabric-ca.json).

The Idemix credential issuance is a two step process. First, send a request with an empty body to the  `/api/v1/idemix/credential`  API endpoint to get a nonce and CA’s Idemix public key. Second, create a credential request using the nonce and CA’s Idemix public key and send another request with the credential request in the body to the  `/api/v1/idemix/credential`  API endpoint to get an Idemix credential, Credential Revocation Information (CRI), and attribute names and values. Currently, only three attributes are supported:

-   **OU**  - organization unit of the user. The value of this attribute is set to user’s affiliation. For example, if user’s affiliaton is  dept1.unit1, then OU attribute is set to  dept1.unit1
-   **IsAdmin**  - if the user is an admin or not. The value of this attribute is set to the value of  isAdmin  registration attribute.
-   **EnrollmentID**  - enrollment ID of the user

You can refer to the  handleIdemixEnroll  function in  [https://github.com/hyperledger/fabric-ca/blob/master/lib/client.go](https://github.com/hyperledger/fabric-ca/blob/master/lib/client.go)  for reference implementation of the two step process for getting Idemix credential.

The  `/api/v1/idemix/credential`  API endpoint accepts both basic and token authorization headers. The basic authorization header should contain User’s registration ID and password. If the user already has X509 enrollment certificate, it can also be used to create a token authorization header.

Note that Hyperledger Fabric will support clients/users to sign transactions with both X509 and Idemix credentials, but will only support X509 credentials for peer and orderer identities. As before, applications can use a Fabric SDK to send requests to the Fabric CA server. SDKs hide the complexity associated with creating authorization header and request payload, and with processing the response.

## Getting Idemix CRI (Certificate Revocation Information)
An Idemix CRI (Credential Revocation Information) is similar in purpose to an X509 CRL (Certificate Revocation List): to revoke what was previously issued. However, there are some differences.

In X509, the issuer revokes an end user’s certificate and its ID is included in the CRL. The verifier checks to see if the user’s certificate is in the CRL and if so, returns an authorization failure. The end user is not involved in this revocation process, other than receiving an authorization error from a verifier.

In Idemix, the end user is involved. The issuer revokes an end user’s credential similar to X509 and evidence of this revocation is recorded in the CRI. The CRI is given to the end user (aka “prover”). The end user then generates a proof that their credential has not been revoked according to the CRI. The end user gives this proof to the verifier who verifies the proof according to the CRI. For verification to succeed, the version of the CRI (known as the “epoch”) used by the end user and verifier must be same. The latest CRI can be requested by sending a request to  `/api/v1/idemix/cri`  API endpoint.

The version of the CRI is incremented when an enroll request is received by the fabric-ca-server and there are no revocation handles remaining in the revocation handle pool. In this case, the fabric-ca-server must generate a new pool of revocation handles which increments the epoch of the CRI. The number of revocation handles in the revocation handle pool is configurable via the  `idemix.rhpoolsize`  server configuration property.

## Reenrolling an Identity
Suppose your enrollment certificate is about to expire or has been compromised. You can issue the reenroll command to renew your enrollment certificate as follows.

    export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca/clients/peer1
    fabric-ca-client reenroll

## Revoking a certificate or identity
An identity or a certificate can be revoked. Revoking an identity will revoke all the certificates owned by the identity and will also prevent the identity from getting any new certificates. Revoking a certificate will invalidate a single certificate.

In order to revoke a certificate or an identity, the calling identity must have the  `hf.Revoker`  and  `hf.Registrar.Roles`  attribute. The revoking identity can only revoke a certificate or an identity that has an affiliation that is equal to or prefixed by the revoking identity’s affiliation. Furthermore, the revoker can only revoke identities with types that are listed in the revoker’s`hf.Registrar.Roles`  attribute.

For example, a revoker with affiliation  **orgs.org1**  and ‘hf.Registrar.Roles=peer,client’ attribute can revoke either a  **peer**  or  **client**  type identity affiliated with  **orgs.org1**  or  **orgs.org1.department1**  but can’t revoke an identity affiliated with  **orgs.org2**  or of any other type.

The following command disables an identity and revokes all of the certificates associated with the identity. All future requests received by the Fabric CA server from this identity will be rejected.

    fabric-ca-client revoke -e <enrollment_id> -r <reason>

The following are the supported reasons that can be specified using  `-r`  flag:

 1.  unspecified
 2.  keycompromise
 3.  cacompromise
 4.  affiliationchange
 5.  superseded
 6.  cessationofoperation
 7.  certificatehold
 8.  removefromcrl
 9.  privilegewithdrawn
 10.  aacompromise

For example, the bootstrap admin who is associated with root of the affiliation tree can revoke **peer1**‘s identity as follows:

    export FABRIC_CA_CLIENT_HOME=$HOME/fabric-ca/clients/admin
    fabric-ca-client revoke -e peer1


An enrollment certificate that belongs to an identity can be revoked by specifying its AKI (Authority Key Identifier) and serial number as follows:

    fabric-ca-client revoke -a xxx -s yyy -r <reason

For example, you can get the AKI and the serial number of a certificate using the openssl command and pass them to the `revoke` command to revoke the said certificate as follows:

    serial=$(openssl x509 -in userecert.pem -serial -noout | cut -d "=" -f 2)
    aki=$(openssl x509 -in userecert.pem -text | awk '/keyid/ {gsub(/ *keyid:|:/,"",$1);print tolower($0)}')
    fabric-ca-client revoke -s $serial -a $aki -r affiliationchange


The –gencrl flag can be used to generate a CRL (Certificate Revocation List) that contains all the revoked certificates. For example, following command will revoke the identity **peer1**, generates a CRL and stores it in the **<msp folder>/crls/crl.pem** file.

    fabric-ca-client revoke -e peer1 --gencrl


    A CRL can also be generated using the gencrl command. Refer to the [Generating a CRL (Certificate Revocation List)](https://hyperledger-fabric-ca.readthedocs.io/en/latest/users-guide.html#generating-a-crl-certificate-revocation-list) section for more information on the gencrl command.


## Generating a CRL (Certificate Revocation List)

After a certificate is revoked in the Fabric CA server, the appropriate MSPs in Hyperledger Fabric must also be updated. This includes both local MSPs of the peers as well as MSPs in the appropriate channel configuration blocks. To do this, PEM encoded CRL (certificate revocation list) file must be placed in the  crls  folder of the MSP. The  `fabric-ca-client  gencrl`  command can be used to generate a CRL. Any identity with  `hf.GenCRL`  attribute can create a CRL that contains serial numbers of all certificates that were revoked during a certain period. The created CRL is stored in the  <msp folder>/crls/crl.pem  file.

The following command will create a CRL containing all the revoked certficates (expired and unexpired) and store the CRL in the  ~/msp/crls/crl.pem  file.

    export FABRIC_CA_CLIENT_HOME=~/clientconfig
    fabric-ca-client gencrl -M ~/msp

The next command will create a CRL containing all certificates (expired and unexpired) that were revoked after 2017-09-13T16:39:57-08:00 (specified by the –revokedafter flag) and before 2017-09-21T16:39:57-08:00 (specified by the –revokedbefore flag) and store the CRL in the ~/msp/crls/crl.pem file.

    export FABRIC_CA_CLIENT_HOME=~/clientconfig
    fabric-ca-client gencrl --caname "" --revokedafter 2017-09-13T16:39:57-08:00 --revokedbefore 2017-09-21T16:39:57-08:00 -M ~/msp

The  –caname  flag specifies the name of the CA to which this request is sent. In this example, the gencrl request is sent to the default CA.

The  –revokedafter  and  –revokedbefore  flags specify the lower and upper boundaries of a time period. The generated CRL will contain certificates that were revoked in this time period. The values must be UTC timestamps specified in RFC3339 format. The  –revokedafter  timestamp cannot be greater than the  –revokedbefore  timestamp.

By default, ‘Next Update’ date of the CRL is set to next day. The  crl.expiry  CA configuration property can be used to specify a custom value.

The gencrl command will also accept –expireafter and –expirebefore flags that can be used to generate a CRL with revoked certificates that expire during the period specified by these flags. For example, the following command will generate a CRL that contains certificates that were revoked after 2017-09-13T16:39:57-08:00 and before 2017-09-21T16:39:57-08:00, and that expire after 2017-09-13T16:39:57-08:00 and before 2018-09-13T16:39:57-08:00:

    export FABRIC_CA_CLIENT_HOME=~/clientconfig
    fabric-ca-client gencrl --caname "" --expireafter 2017-09-13T16:39:57-08:00 --expirebefore 2018-09-13T16:39:57-08:00  --revokedafter 2017-09-13T16:39:57-08:00 --revokedbefore 2017-09-21T16:39:57-08:00 -M ~/msp


## Enabling TLS
This section describes in more detail how to configure TLS for a Fabric CA client.

The following sections may be configured in the `fabric-ca-client-config.yaml`.

    tls:
      # Enable TLS (default: false)
      enabled: true
      certfiles:
        - root.pem
      client:
        certfile: tls_client-cert.pem
        keyfile: tls_client-key.pem



The  **certfiles**  option is the set of root certificates trusted by the client. This will typically just be the root Fabric CA server’s certificate found in the server’s home directory in the  **ca-cert.pem**file.

The  **client**  option is required only if mutual TLS is configured on the server.

## Attribute-Based Access Control
Access control decisions can be made by chaincode (and by the Hyperledger Fabric runtime) based upon an identity’s attributes. This is called  **Attribute-Based Access Control**, or  **ABAC**  for short.

In order to make this possible, an identity’s enrollment certificate (ECert) may contain one or more attribute name and value. The chaincode then extracts an attribute’s value to make an access control decision.

For example, suppose that you are developing application  _app1_  and want a particular chaincode operation to be accessible only by app1 administrators. Your chaincode could verify that the caller’s certificate (which was issued by a CA trusted for the channel) contains an attribute named  _app1Admin_  with a value of  _true_. Of course the name of the attribute can be anything and the value need not be a boolean value.

So how do you get an enrollment certificate with an attribute? There are two methods:
1. When you register an identity, you can specify that an enrollment certificate issued for the identity should by default contain an attribute. This behavior can be overridden at enrollment time, but this is useful for establishing default behavior and, assuming registration occurs outside of your application, does not require any application change.

	The following shows how to register  _user1_  with two attributes:  _app1Admin_  and  _email_. The ”:ecert” suffix causes the  _appAdmin_  attribute to be inserted into user1’s enrollment certificate by default, when the user does not explicitly request attributes at enrollment time. The  _email_  attribute is not added to the enrollment certificate by default.
	
		fabric-ca-client register --id.name user1 --id.secret user1pw --id.type user --id.affiliation org1 --id.attrs 'app1Admin=true:ecert,email=user1@gmail.com'

2. When you enroll an identity, you may explicitly request that one or more attributes be added to the certificate. For each attribute requested, you may specify whether the attribute is optional or not. If it is not requested optionally and the identity does not possess the attribute, an error will occur.

	The following shows how to enroll  _user1_  with the  _email_  attribute, without the  _app1Admin_attribute, and optionally with the  _phone_  attribute (if the user possesses the  _phone_  attribute).
	
		fabric-ca-client enroll -u http://user1:user1pw@localhost:7054 --enrollment.attrs "email,phone:opt"


The table below shows the three attributes which are automatically registered for every identity.
**Attribute Name**|**Attribute Value**
:-----:|:-----:
hf.EnrollmentID|The enrollment ID of the identity
hf.Type|The type of the identity
hf.Affiliation|The affiliation of the identity


To add any of the above attributes **by default** to a certificate, you must explicitly register the attribute with the ”:ecert” specification. For example, the following registers identity ‘user1’ so that the ‘hf.Affiliation’ attribute will be added to an enrollment certificate if no specific attributes are requested at enrollment time. Note that the value of the affiliation (which is ‘org1’) must be the same in both the ‘–id.affiliation’ and the ‘–id.attrs’ flags.


    fabric-ca-client register --id.name user1 --id.secret user1pw --id.type user --id.affiliation org1 --id.attrs 'hf.Affiliation=org1:ecert'

For information on the chaincode library API for Attribute-Based Access Control, see [https://github.com/hyperledger/fabric/blob/master/core/chaincode/lib/cid/README.md](https://github.com/hyperledger/fabric/blob/master/core/chaincode/lib/cid/README.md)


## Dynamic Server Configuration Update
This section describes how to use fabric-ca-client to dynamically update portions of the fabric-ca-server’s configuration without restarting the server.

All commands in this section require that you first be enrolled by executing the  fabric-ca-client enroll  command.

### Dynamically updating identities
An authorization failure will occur if the client identity does not satisfy all of the following:

 -   The client identity must possess the “hf.Registrar.Roles” attribute with a comma-separated list of values where one of the values equals the type of identity being updated; for example, if the client’s identity has the “hf.Registrar.Roles” attribute with a value of “client,peer”, the client can update identities of type ‘client’ and ‘peer’, but not ‘orderer’.
 -   The affiliation of the client’s identity must be equal to or a prefix of the affiliation of the identity being updated. For example, a client with an affiliation of “a.b” may update an identity with an affiliation of “a.b.c” but may not update an identity with an affiliation of “a.c”. If root affiliation is required for an identity, then the update request should specify a dot (”.”) for the affiliation and the client must also have root affiliation.

The following shows how to add, modify, and remove an affiliation.

#### Getting Identity Information
A caller may retrieve information on a identity from the fabric-ca server as long as the caller meets the authorization requirements highlighted in the section above. The following command shows how to get an identity:

    fabric-ca-client identity list --id user1

A caller may also request to retrieve information on all identities that it is authorized to see by issuing the following command:

    fabric-ca-client identity list

#### Adding an identity
The following adds a new identity for ‘user1’. Adding a new identity performs the same action as registering an identity via the ‘fabric-ca-client register’ command. There are two available methods for adding a new identity. The first method is via the –json flag where you describe the identity in a JSON string.

    fabric-ca-client identity add user1 --json '{"secret": "user1pw", "type": "user", "affiliation": "org1", "max_enrollments": 1, "attrs": [{"name": "hf.Revoker", "value": "true"}]}'

The following adds a user with root affiliation. Note that an affiliation name of ”.” means the root affiliation:

    fabric-ca-client identity add user1 --json '{"secret": "user1pw", "type": "user", "affiliation": ".", "max_enrollments": 1, "attrs": [{"name": "hf.Revoker", "value": "true"}]}'


The second method for adding an identity is to use direct flags. See the example below for adding ‘user1’:

    fabric-ca-client identity add user1 --secret user1pw --type user --affiliation . --maxenrollments 1 --attrs hf.Revoker=true


The table below lists all the fields of an identity and whether they are required or optional, and any default values they might have.

**Fields**|**Required**|**Default Value**
:-----:|:-----:|:-----:
ID|Yes| 
Secret|No| 
Affiliation|No|Caller’s Affiliation
Type|No|client
Maxenrollments|No|0
Attributes|No| 

#### Modifying an identity
There are two available methods for modifying an existing identity. The first method is via the –json flag where you describe the modifications in to an identity in a JSON string. Multiple modifications can be made in a single request. Any element of an identity that is not modified will retain its original value.

> NOTE: A maxenrollments value of “-2” specifies that the CA’s max
> enrollment setting is to be used.

he command below make multiple modification to an identity using the –json flag:

    fabric-ca-client identity modify user1 --json '{"secret": "newPassword", "affiliation": ".", "attrs": [{"name": "hf.Regisrar.Roles", "value": "peer,client"},{"name": "hf.Revoker", "value": "true"}]}'

The commands below make modifications using direct flags. The following updates the enrollment secret (or password) for identity ‘user1’ to ‘newsecret':

    fabric-ca-client identity modify user1 --secret newsecret

The following updates the affiliation of identity ‘user1’ to ‘org2’:

    fabric-ca-client identity modify user1 --affiliation org2

The following updates the type of identity ‘user1’ to ‘peer’:

    fabric-ca-client identity modify user1 --type peer


The following updates the maxenrollments of identity ‘user1’ to 5:

    fabric-ca-client identity modify user1 --maxenrollments 5


By specifying a maxenrollments value of ‘-2’, the following causes identity ‘user1’ to use the CA’s max enrollment setting:


    fabric-ca-client identity modify user1 --maxenrollments -2


The following sets the value of the ‘hf.Revoker’ attribute for identity ‘user1’ to ‘false’. If the identity has other attributes, they are not changed. If the identity did not previously possess the ‘hf.Revoker’ attribute, the attribute is added to the identity. An attribute may also be removed by specifying no value for the attribute.

    fabric-ca-client identity modify user1 --attrs hf.Revoker=false

The following removes the ‘hf.Revoker’ attribute for user ‘user1’.

    fabric-ca-client identity modify user1 --attrs hf.Revoker=


The following demonstrates that multiple options may be used in a single fabric-ca-client identity modify command. In this case, both the secret and the type are updated for user ‘user1’:

    fabric-ca-client identity modify user1 --secret newpass --type peer


#### Removing an identity
The following removes identity ‘user1’ and also revokes any certificates associated with the ‘user1’ identity:

    fabric-ca-client identity remove user1

> Removal of identities is disabled in the fabric-ca-server by default,
> but may be enabled by starting the fabric-ca-server with the
> –cfg.identities.allowremove option.


### Dynamically updating affiliations
This section describes how to use fabric-ca-client to dynamically update affiliations. The following shows how to add, modify, remove, and list an affiliation.

#### Adding an affiliation
An authorization failure will occur if the client identity does not satisfy all of the following:

 -   The client identity must possess the attribute ‘hf.AffiliationMgr’ with a value of ‘true’.
 -   The affiliation of the client identity must be hierarchically above the affiliation being updated. For example, if the client’s affiliation is “a.b”, the client may add affiliation “a.b.c” but not “a” or “a.b”.

The following adds a new affiliation named ‘org1.dept1’.

    fabric-ca-client affiliation add org1.dept1


#### Modifying an affiliation
An authorization failure will occur if the client identity does not satisfy all of the following:
-   The client identity must possess the attribute ‘hf.AffiliationMgr’ with a value of ‘true’.
-   The affiliation of the client identity must be hierarchically above the affiliation being updated. For example, if the client’s affiliation is “a.b”, the client may add affiliation “a.b.c” but not “a” or “a.b”.
-   If the ‘–force’ option is true and there are identities which must be modified, the client identity must also be authorized to modify the identity.
- 
The following renames the ‘org2’ affiliation to ‘org3’. It also renames any sub affiliations (e.g. ‘org2.department1’ is renamed to ‘org3.department1’):

    fabric-ca-client affiliation modify org2 --name org3


If there are identities that are affected by the renaming of an affiliation, it will result in an error unless the ‘–force’ option is used. Using the ‘–force’ option will update the affiliation of identities that are affected to use the new affiliation name.

    fabric-ca-client affiliation modify org1 --name org2 --force


#### Removing an affiliation
An authorization failure will occur if the client identity does not satisfy all of the following:

 -   The client identity must possess the attribute ‘hf.AffiliationMgr’ with a value of ‘true’.
 -   The affiliation of the client identity must be hierarchically above the affiliation being updated. For example, if the client’s affiliation is “a.b”, the client may remove affiliation “a.b.c” but not “a” or “a.b”.
 -   If the ‘–force’ option is true and there are identities which must be modified, the client identity must also be authorized to modify the identity.
 - 
The following removes affiliation ‘org2’ and also any sub affiliations. For example, if ‘org2.dept1’ is an affiliation below ‘org2’, it is also removed:

    fabric-ca-client affiliation remove org2

If there are identities that are affected by the removing of an affiliation, it will result in an error unless the ‘–force’ option is used. Using the ‘–force’ option will also remove all identities that are associated with that affiliation, and the certificates associated with any of these identities.

    Removal of affiliations is disabled in the fabric-ca-server by default, but may be enabled by starting the fabric-ca-server with the –cfg.affiliations.allowremove option.


### Listing affiliation information
An authorization failure will occur if the client identity does not satisfy all of the following:

 -   The client identity must possess the attribute ‘hf.AffiliationMgr’ with a value of ‘true’.
 -   Affiliation of the client identity must be equal to or be hierarchically above the affiliation being updated. For example, if the client’s affiliation is “a.b”, the client may get affiliation information on “a.b” or “a.b.c” but not “a” or “a.c”.

The following command shows how to get a specific affiliation.

    fabric-ca-client affiliation list --affiliation org2.dept1


The following command shows how to get a specific affiliation:

    fabric-ca-client affiliation list --affiliation org2.dept1


A caller may also request to retrieve information on all affiliations that it is authorized to see by issuing the following command:

    fabric-ca-client affiliation list

## Manage Certificates

This section describes how to use fabric-ca-client to manage certificates.

### Listing certificate information
The certificates that are visible to a caller include:
-   Those certificates which belong to the caller
-   If the caller possesses the  `hf.Registrar.Roles`  attribute or the  `hf.Revoker`  attribute with a value of  `true`, all certificates which belong to identities in and below the caller’s affiliation. For example, if the client’s affiliation is  `a.b`, the client may get certificates for identities who’s affiliation is  `a.b`  or  `a.b.c`  but not  `a`  or  `a.c`.

If executing a list command that requests certificates of more than one identity, only certificates of identities with an affiliation that is equal to or hierarchically below the caller’s affiliation will be listed.

The certificates which will be listed may be filtered based on ID, AKI, serial number, expiration time, revocation time, notrevoked, and notexpired flags:
-   `id`: List certificates for this enrollment ID
-   `serial`: List certificates that have this serial number
-   `aki`: List certificates that have this AKI
-   `expiration`: List certificates that have expiration dates that fall within this expiration time
-   `revocation`: List certificates that were revoked within this revocation time
-   `notrevoked`: List certificates that have not yet been revoked
-   `notexpired`: List certificates that have not yet expired


You can use flags `notexpired` and `notrevoked` as filters to exclude revoked certificates and/or expired certificates from the result set. For example, if you only care about certificates that have expired but have not been revoked you can use the `expiration` and `notrevoked` flags to get back such results. An example of this case is provided below.

Time should be specified based on RFC3339. For instance, to list certificates that have expirations between March 1, 2018 at 1:00 PM and June 15, 2018 at 2:00 AM, the input time string would look like 2018-03-01T13:00:00z and 2018-06-15T02:00:00z. If time is not a concern, and only the dates matter, then the time part can be left off and then the strings become 2018-03-01 and 2018-06-15.

The string `now` may be used to denote the current time and the empty string to denote any time. For example, `now::` denotes a time range from now to any time in the future, and `::now` denotes a time range from any time in the past until now.

The following commands show how to list certificates using various filters.

List all certificates:

    fabric-ca-client certificate list



List all certificates by id:

    fabric-ca-client certificate list --id admin

List certificate by serial and aki:

    fabric-ca-client certificate list --serial 1234 --aki 1234

List certificate by id and serial/aki:

    fabric-ca-client certificate list --id admin --serial 1234 --aki 1234

List certificates that are neither revoker nor expired by id:

    fabric-ca-client certificate list --id admin --notrevoked --notexpired

List all certificates that have not been revoked for an id (admin):

    fabric-ca-client certificate list --id admin --notrevoked

List all certificates have not expired for an id (admin):

The “–notexpired” flag is equivalent to “–expiration now::”, which means certificates will expire some time in the future.

    fabric-ca-client certificate list --id admin --notexpired

List all certificates that were revoked between a time range for an id (admin):

    fabric-ca-client certificate list --id admin --revocation 2018-01-01T01:30:00z::2018-01-30T05:00:00z

List all certificates that were revoked between a time range but have not expired for an id (admin):

    fabric-ca-client certificate list --id admin --revocation 2018-01-01::2018-01-30 --notexpired

List all revoked certificates using duration (revoked between 30 days and 15 days ago) for an id (admin):

    fabric-ca-client certificate list --id admin --revocation -30d::-15d

List all revoked certificates before a time

    fabric-ca-client certificate list --revocation ::2018-01-30

List all revoked certificates after a time

    fabric-ca-client certificate list --revocation 2018-01-30::

List all revoked certificates before now and after a certain date

    fabric-ca-client certificate list --id admin --revocation 2018-01-30::now

List all certificate that expired between a time range but have not been revoked for an id (admin):

    fabric-ca-client certificate list --id admin --expiration 2018-01-01::2018-01-30 --notrevoked

List all expired certificates using duration (expired between 30 days and 15 days ago) for an id (admin):

    fabric-ca-client certificate list --expiration -30d::-15d

List all certificates that have expired or will expire before a certain time

    fabric-ca-client certificate list --expiration ::2058-01-30

List all certificates that have expired or will expire after a certain time

    fabric-ca-client certificate list --expiration 2018-01-30::

List all expired certificates before now and after a certain date

    fabric-ca-client certificate list --expiration 2018-01-30::now

List certificates expiring in the next 10 days:

    fabric-ca-client certificate list --id admin --expiration ::+10d --notrevoked

The list certificate command can also be used to store certificates on the file system. This is a convenient way to populate the admins folder in an MSP, The “-store” flag points to the location on the file system to store the certificates.

Configure an identity to be an admin, by storing certificates for an identity in the MSP:

    export FABRIC_CA_CLIENT_HOME=/tmp/clientHome
    fabric-ca-client certificate list --id admin --store msp/admincerts




## Contact specific CA instance
When a server is running multiple CA instances, requests can be directed to a specific CA. By default, if no CA name is specified in the client request the request will be directed to the default CA on the fabric-ca server. A CA name can be specified on the command line of a client command using the `caname` filter as follows:

    fabric-ca-client enroll -u http://admin:adminpw@localhost:7054 --caname <caname>