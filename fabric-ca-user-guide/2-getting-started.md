## Getting Started

### Prerequisites

-   Go 1.10+ installation
-   `GOPATH`  environment variable is set correctly
-   libtool and libtdhl-dev packages are installed

The following installs the libtool dependencies on Ubuntu:

    sudo apt install libtool libltdl-dev

The following installs the libtool dependencies on MacOSX:

    brew install libtool

> libtldl-dev is not necessary on MacOSX if you instal libtool via
> Homebrew

For more information on libtool, see  [https://www.gnu.org/software/libtool](https://www.gnu.org/software/libtool).

For more information on libltdl-dev, see  [https://www.gnu.org/software/libtool/manual/html_node/Using-libltdl.html](https://www.gnu.org/software/libtool/manual/html_node/Using-libltdl.html).

### Install
The following installs both the fabric-ca-server and fabric-ca-client binaries in $GOPATH/bin.

    go get -u github.com/hyperledger/fabric-ca/cmd/...


Note: If you have already cloned the fabric-ca repository, make sure you are on the master branch before running the ‘go get’ command above. Otherwise, you might see the following error:

    <gopath>/src/github.com/hyperledger/fabric-ca; git pull --ff-only
    There is no tracking information for the current branch.
    Please specify which branch you want to merge with.
    See git-pull(1) for details.
    
        git pull <remote> <branch>
    
    If you wish to set tracking information for this branch you can do so with:
    
        git branch --set-upstream-to=<remote>/<branch> tlsdoc
    
    package github.com/hyperledger/fabric-ca/cmd/fabric-ca-client: exit status 1


### Start Server Natively
The following starts the fabric-ca-server with default settings.

    fabric-ca-server start -b admin:adminpw

The -b option provides the enrollment ID and secret for a bootstrap administrator; this is required if LDAP is not enabled with the “ldap.enabled” setting.

A default configuration file named fabric-ca-server-config.yaml is created in the local directory which can be customized.

### Start Server via Docker
#### Docker Hub
Go to: [https://hub.docker.com/r/hyperledger/fabric-ca/tags/](https://hub.docker.com/r/hyperledger/fabric-ca/tags/)

Go to:  [https://hub.docker.com/r/hyperledger/fabric-ca/tags/](https://hub.docker.com/r/hyperledger/fabric-ca/tags/)

Find the tag that matches the architecture and version of fabric-ca that you want to pull.

Navigate to  $GOPATH/src/github.com/hyperledger/fabric-ca/docker/server  and open up docker-compose.yml in an editor.

Change the  image  line to reflect the tag you found previously. The file may look like this for an x86 architecture for version beta.

    fabric-ca-server:
      image: hyperledger/fabric-ca:x86_64-1.0.0-beta
      container_name: fabric-ca-server
      ports:
        - "7054:7054"
      environment:
        - FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server
      volumes:
        - "./fabric-ca-server:/etc/hyperledger/fabric-ca-server"
      command: sh -c 'fabric-ca-server start -b admin:adminpw'

Open up a terminal in the same directory as the docker-compose.yml file and execute the following:

    docker-compose up -d


This will pull down the specified fabric-ca image in the compose file if it does not already exist, and start an instance of the fabric-ca server.

#### Building Your Own Docker image
You can build and start the server via docker-compose as shown below.

    cd $GOPATH/src/github.com/hyperledger/fabric-ca
    make docker
    cd docker/server
    docker-compose up -d

The hyperledger/fabric-ca docker image contains both the fabric-ca-server and the fabric-ca-client.

### Explore the Fabric CA CLI
This section simply provides the usage messages for the Fabric CA server and client for convenience. Additional usage information is provided in following sections.

The following links shows the  [Server Command Line](https://hyperledger-fabric-ca.readthedocs.io/en/latest/servercli.html)  and  [Client Command Line](https://hyperledger-fabric-ca.readthedocs.io/en/latest/clientcli.html).

> Note that command line options that are string slices (lists) can be specified either by specifying the option with comma-separated list elements or by specifying the option multiple times, each with a string value that make up the list. For example, to specify `host1`and `host2` for the `csr.hosts` option, you can either pass `--csr.hosts  'host1,host2'` or`--csr.hosts  host1  --csr.hosts  host2`. When using the former format, please make sure there are no space before or after any commas.

### Configuration Settings
The Fabric CA provides 3 ways to configure settings on the Fabric CA server and client. The precedence order is:

> 1.  CLI flags
> 2.  Environment variables
> 3.  Configuration file


In the remainder of this document, we refer to making changes to configuration files. However, configuration file changes can be overridden through environment variables or CLI flags.

For example, if we have the following in the client configuration file:

    tls:
      # Enable TLS (default: false)
      enabled: false
    
      # TLS for the client's listenting port (default: false)
      certfiles:
      client:
        certfile: cert.pem
        keyfile:


The following environment variable may be used to override the `cert.pem` setting in the configuration file:

    export FABRIC_CA_CLIENT_TLS_CLIENT_CERTFILE=cert2.pem


If we wanted to override both the environment variable and configuration file, we can use a command line flag.

    fabric-ca-client enroll --tls.client.certfile cert3.pem


The same approach applies to fabric-ca-server, except instead of using `FABIRC_CA_CLIENT` as the prefix to environment variables, `FABRIC_CA_SERVER` is used.

#### A word on file paths
All the properties in the Fabric CA server and client configuration file that specify file names support both relative and absolute paths. Relative paths are relative to the config directory, where the configuration file is located. For example, if the config directory is `~/config` and the tls section is as shown below, the Fabric CA server or client will look for the `root.pem` file in the `~/config` directory, `cert.pem` file in the `~/config/certs` directory and the `key.pem` file in the `/abs/path` directory.

    tls:
      enabled: true
      certfiles:
        - root.pem
      client:
        certfile: certs/cert.pem
        keyfile: /abs/path/key.pem