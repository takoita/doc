# HSM(Harward Security Module)
By default, the Fabric CA server and client store private keys in a PEM-encoded file, but they can also be configured to store private keys in an HSM (Hardware Security Module) via PKCS11 APIs. This behavior is configured in the BCCSP (BlockChain Crypto Service Provider) section of the server’s or client’s configuration file.

## Configuring Fabric CA server to use softhsm2
First  install [softhsm2](https://github.com/opendnssec/SoftHSMv2):  

    git clone https://github.com/opendnssec/SoftHSMv2.git    
    sudo apt-get install pkg-config --yes
    sudo apt-get install autoconf --yes
	sudo apt-get install  libssl-dev --yes
    sudo apt-get install libp11-kit-dev 
    sudo apt-get install libcppunit-dev    
    
    sh autogen.sh
    ./configure
    make
    make check
	sudo make install

After installing softhsm, make sure to set your SOFTHSM2_CONF environment variable to point to the location where the softhsm2 configuration file is stored. The config file looks like:

    directories.tokendir = /tmp/
    objectstore.backend = file
    log.level = INFO

You can find example configuration file named softhsm2.conf under testdata directory.

Create a token, label it “ForFabric”, set the pin to ‘98765432’ (refer to softhsm documentation):

    softhsm2-util --init-token --slot 0 --label "ForFabric"

You can use both the config file and environment variables to configure BCCSP For example, set the bccsp section of Fabric CA server configuration file as follows. Note that the default field’s value is PKCS11.

    #############################################################################
    # BCCSP (BlockChain Crypto Service Provider) section is used to select which
    # crypto library implementation to use
    #############################################################################
    bccsp:
      default: PKCS11
      pkcs11:
        Library: /usr/local/Cellar/softhsm/2.1.0/lib/softhsm/libsofthsm2.so
        Pin: 98765432
        Label: ForFabric
        hash: SHA2
        security: 256
        filekeystore:
          # The directory used for the software file-based keystore
          keystore: msp/keystore


And you can override relevant fields via environment variables as follows:

    FABRIC_CA_SERVER_BCCSP_DEFAULT=PKCS11
    FABRIC_CA_SERVER_BCCSP_PKCS11_LIBRARY=/usr/local/Cellar/softhsm/2.1.0/lib/softhsm/libsofthsm2.so
    FABRIC_CA_SERVER_BCCSP_PKCS11_PIN=98765432
    FABRIC_CA_SERVER_BCCSP_PKCS11_LABEL=ForFabric